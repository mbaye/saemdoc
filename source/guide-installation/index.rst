.. _installation:
.. index:: guide-installation

============
Guide d'installation
============

Le Système d’Archivage Electronique Mutualisé, SAEM est constitué de trois applications :
1. un référentiel de donnée développé par la société Logilab ;
2. un espace d’échange reposant sur Alfresco, un outil de GED open source ;
3. une solution d’archivage réalisé par Libriciel.

Afin de faciliter le déploiement et la mise à jour de ces applications, le CD33 a décidé d’utiliser Saltstack. Il s’agit d’un logiciel de gestion de configuration permettant d’installer des applications et d’exécuter des commandes sur des machines distantes.
Pour plus d’information sur Salt, il faudra consulter le site officiel à l’adresse <https://saltstack.com/>.
Ce document décrit les actions à effectuer pour installer et mettre à jour les différents composants de la solution SAEM sur une distribution CentOs ou Redhat.

Convention
-------------
Dans la suite du document la variable ``"${SAEM_SALT}"`` fera référence au dossier contenant l’ensemble des livrables nécessaires à l’installation des différents modules.
Les éléments coloriés en gras sont mentionnés à titre d’exemple et devront être adapté en fonction du contexte.

Fichiers de Configuration SaltStack (Master/Minion)
-----------------

L’ensemble des fichiers de configuration SaltStack à modifier sont décrits dans les paragraphes suivants.
Attention : Les éléments mentionnés via la mise en forme suivante : valeur devront être adaptées à l’environnement cible.

2.3.1 Configuration du Master SaltStack

* /etc/salt/master

.. code-block:: text

  ##### Primary configuration settings #####
  ##########################################
  # The address of the interface to bind to:
  # interface: 0.0.0.0
  # interface: 10.255.234.39
  # Whether the master should listen for IPv6 connections. If this is set to True,
  # the interface option must be adjusted, too. (For example: "interface: '::'")
  # ipv6: False
  # Set the default timeout for the salt command and api. The default is 5
  # seconds.
  # timeout: 900

.. code-block:: text

* /etc/salt/master.d/master.conf
  file_roots:
    base:
      - /srv/salt/
    prod:
      - /srv/salt/
  fileserver_backend:
    - roots
  pillar_roots:
    base:
      - /srv/pillar/base
    prod:
      - /srv/pillar/prod

Configuration des Minions SaltStack
--------------------------

.. code-block:: text

*  /etc/salt/minion
  ##### Primary configuration settings #####
  ##########################################
  # This configuration file is used to manage the behavior of the Salt Minion.
  # With the exception of the location of the Salt Master Server, values that are
  # commented out but have an empty line after the comment are defaults that need
  # not be set in the config. If there is no blank line after the comment, the
  # value is presented as an example and is not the default.
  # Per default the minion will automatically include all config files
  # from minion.d/*.conf (minion.d is a directory in the same directory
  # as the main minion config file).
  #default_include: minion.d/*.conf
  # Set the location of the salt master server. If the master server cannot be
  # resolved, then the minion will fail to start.
  master: **100.255.111.74**
  # Set http proxy information for the minion when doing requests
  proxy_host: **localhost**
  proxy_port: **3000**
  # Set whether the minion should connect to the master via IPv6:
  ipv6: False

Minion Alfresco
---------------

Configuration du Minion Alfresco (/srv/pillar/base/gedsas/init.sls)

.. code-block:: text

  alfresco:
  # Emplacement du dossier data
    data: **/opt/alfresco-5.0.d/alf_data**
  # Nom DNS du serveur ou est installe Alfresco
    host: **alias.cg33.xyz**
  # Parametres de la base de donnees
    db:
  # Machine et port du serveur de base de donnees
      host: **10.10.10.1**
      port: **9999**
  # Nom de la base
      name: **saem-nomdelabase**
  # Identifiants pour acceder a la base
      username: **username**
      password: ********
    mail:
      host: **alias.cg33.fr**
      port: 25
      username: XXXX
      password: YYYY
      from:
        default: no-reply@ged.cg33.fr
      auth: false

    ldap:
      enabled: true
    authentication:
      active: true
      allowGuestLogin: false


      userNameFormat: **'%s@cg33.fr'**
      url: **ldap://xxx.cg33.fr:389**
      defaultAdministratorUserNames: **AZY**
      principal: **admin**
      credentials: **secret**
    synchronization:
      groupDifferentialQuery: **(&(objectclass\=group)(!(modifyTimestamp<\={0})))**
      personDifferentialQuery: **(&(objectclass\=user)(userAccountControl\:1.2.840.113556.1.4.803\:\=512)(!(modifyTimestamp<\={0})))**
      groupSearchBase: **ou\=GED-CG33,ou\=Groupes Applications,ou\=Groupes,ou\=CGG,dc=cg33,dc=fr**
      userSearchBase: **ou\=Utilisateurs,ou\=CGG,dc=cg33,dc=fr**
      modifyTimestampAttributeName: **modifyTimestamp**
      syncOnStartup: false
      # Nom DNS du serveur ou est installe la webapp share
      share:
        host: srvmv-web030.cg33.dmz2
      # Parametres du referentiel de logilab
        referentiel:
      # URL du referentiel
        url: http://10.255.231.23:18080
      # Token et secret pour authentification HMAC
        token: id: 3ddfe77d331d48ed9e803b3c2f8a81ce
        secret: a0380072300843368177c9833dc38be6a6931a23e9e74c1eb3ba8bff91512642f84c8a2529384fcc983993e957f5fb0aa7e3707b0d18472db13d29fea96ff3aa
      # Parametres du job de synchro avec le referentiel
        synchro:
      # cron de lancement de la synchro , chaque trois heures ci-dessous
        cronExpression: 0 30 */3 * * ?
      # Parametres de asalae
      asalae:
        url: **http://10.10.10.1**
        login: **admin**
        password: **admin**
      # Parametres du job de recuperation des messages seda d asalae
        poller:
      # cron de lancement du job , chaque deux minutes ci-dessous
        cronExpression: 0 */2 * * * ?

Minion As@lae
------------------

Configuration du Minion As@ale (/srv/pillar/base/asalae/init.sls)

.. code-block:: text

  asalae:
    lookup:
      dist:
        url: **salt://asalae/files/asalae1.6.4_cakephp1.2.10.zip**
        hash: **md5=fdfca2398b9b27a065608ffc34c95c50**
        name: **asalae1.6.4_cakephp1.2.10**
      db:
        host: **'10.10.10.1'**
        database: **'databasename'**
        user: **'username'**
        login: **'login'**
        password: **'********'**
        port: '1111'
      proxy:
        http: **'http://localhost:3000'**
        https: **'https://localhost:3000'**
      referentiel:
        url: **http://10.10.10.2:18080**
      token:
        id: **ad3ddfe7dflkgh569e803b36876d81cemin**
        secret: **a0380072300843368177csdkljhfdskljhflkjh4536354f91512642f84clkjhlkjshf564sgd5g757e3707b0d18472db13d29fea96ff3aa**

Minion Référentiel
------------------------

Configuration du Minion Référentiel (/srv/pillar/base/saemref/init.sls)

.. code-block:: text

  saemref:
    lookup:
      instance:
        # nom instance referentiel
        name: saemref
        # nom utilisateur systeme
        user: saemref
        # http port
        port: 18080
        oai_port: 18081
        oai_threads: 8
        # Anonymous user
        anonymous_user: **anon**
        anonymous_password: **anon**
        # wsgi settings
        # 2 * 8 = can handle 16 concurrent request
        # and will use 16 database connections.
        wsgi: true
        wsgi_workers: 2
        wsgi_threads: 8
        # Secret keys for encrypting session/cookies data and authentication They
        # are MANDATORY and should not be the same.
        sessions_secret: SETME
        authtk_session_secret: SETME1
        authtk_persistent_secret: SETME2
        # Pool size must be equal to wsgi_threads when using wsgi
        pool_size: 8
        # Run in test mode: will replace some data files to create the instance faster
        test_mode: true
        db:
          driver: postgres
          host: **"10.10.10.2"**
          port: **"9999"**
          name: **name**
          user: **username**
          pass: **password**
          admin:
            login: **admin**
            pass: **admin**
        postgres:
          version: 9.4

Installation de Salt
-------------------

Saltstack fonctionne de manière centralisée à partir du modèle « maître/esclave ».
La terminologie utilisée dans Salt pour désigner le « maître » est « salt-master », un « esclave » est appelé « salt-minion ».

Saltstack repository
-----------------------

La liste des paquets « Salt » est accessible à partir du « repository saltstack ».Pour l’ajouter à la liste des dépôts salt, il faut exécuter les commandes suivantes avec un compte root sur chacune des machines où salt sera installé.
Exécuter les commandes suivantes.

.. code-block:: text
    $ yum clean expire-cache
    $ yum update

Pour une machine reposant sur Redhat/CentOs7


    $ yum install https://repo.saltstack.com/yum/redhat/salt-repo-2016.11-2.el7.noarch.rpm


Pour une machine reposant sur Redhat/CentOs6

    $ yum install https://repo.saltstack.com/yum/redhat/salt-repo-2016.11-2.el6.noarch.rpm


Salt Master
----------------------------
Installation
--------------
Se connecter sur la machine cible avec un compte root
Saisir les commandes ci-dessous.

.. code-block:: text
    $ yum install salt-master
    $ chkconfig salt-master on

Configuration
-----------

1. Copier le fichier master.conf fourni à l’emplacement et /etc/salt/master.d/.


    $ cp ${SAEM_SALT}/config/master.conf /etc/salt/master.d/master.conf

2. Modifier le timeout à 900 dans le fichier /etc/salt/master
3. Copier le contenu du dossier ${SAEM_SALT}/srv/ sous /srv/
4. Vérifier que le dossier /srv/salt/ contient les sous-dossiers suivants

.. code-block:: text
    $ ls -1 /srv/salt/
    asalae
    common
    gedsas
    referentiel
    top.sls


5. Editer le fichier /srv/salt/top.sls suivant le modèle ci-dessous

  base :
    'referentiel':
      saemref
    'alfresco':
      gedsas
    'asalae':
      asalae

Attention : Les chaînes “referentiel.*”, “alfresco.*” et “asalae.*” devront être remplacées par le nom des machines où seront hébergées les applications cibles.

6. Editer le fichier /srv/pillar/base/top.sls en suivant le modèle précédent.

Arrêt/Relance du Salt Master
------------------------------
* Arrêter le master

    $ service salt-master stop

** Démarrer le master

    $ service salt-master start

3.3 Salt Minion(s)
----------------------

3.3.1 Installation
--------------------
Se connecter sur les machines cibles avec un compte root et saisir les commandes suivantes.

  $ yum install salt-minion
  $ chkconfig salt-minion on

  3.3.2 Configuration
  --------------------
  1. Editer le fichier /etc/salt/minion

Localiser la propriété « master » :

Remplacer #master : salt par master :IP_MASTER où IP_MASTER désigne l’adresse IP du master.

2. Si le minion utilise un proxy pour accéder à internet

a. Décommenter et renseigner les paramètres proxy_host et proxy_port.

b. Si besoin de spécifier un proxy, créer le fichier /root/.curlrc et ajouter la ligne ci-dessous avec les paramètres adéquats
    proxy=PROXY_HOST:PROXY_PORT

c. Modifier le fichier /root/.bashrc et ajouter la ligne ci-dessous avec les paramètres adéquats

  $ export http_proxy=http://111.11.11.1:8080
  $ export https_proxy=http://112.11.11.1:8080

d. Ajouter les proxys dans les formulas SALT

3.3.3 Arrêt/Relance Salt Minion(s)
--------------------------------

Arrêter le minion.

  $ service salt-minion stop

Démarrer le minion.

  $ service salt-minion start

  3.4 Tester la communication entre master et minions
  ------------------------------

  PRE-REQUIS : le master et les minions doivent être démarrés.

Pour vérifier la liste des minions exécuter la commande suivante sur le master.


  $ salt-key –L
  Pour accepter un minion ayant la clé KEY.
    $ salt-key -a KEY
  Pour rejeter un minion ayant la clé KEY.
    $ salt-key -d KEY
  Pour tester la communication entre minions et master, exécuter la commande sur le master.
    $ salt '*' test.ping

## 4 Installation de la solution SAEM
----------------------

4.1 Base de données & Serveur PostgreSQL
-----------------------------

L’instanciation des bases de données nécessaires à la solution SAEM ainsi que des rôles associés seront à la charge de l’exploitant suivant les bonnes pratiques du service d’Exploitation.

Le serveur de base de données doit être accessibles à partir des différents minions. Il pourra donc être nécessaire d’éditer le fichier pg_hba .conf.

4.2 Description des formules SaltStack
--------------------------------

4.2.1 Formula « saemref »
---------------------------
Logilab met à disposition une formule pour l’installation du référentiel. La version courante est accessible sur un dépôt Git à https://framagit.org/saemproject/saemref-formula.

Cette formule est composée de plusieurs « states » :
1. saemref.init : Détermine s’il agit d’une installation ou d’une mise à jour
  a. saemref.install : mise à jour des paquets Cubicweb
  b. saemref.client : configuration du client
  c. saemref.config : mise à jour de la configuration
  d. saemref.db-create : création de la base de données
  e. saemref.supervisor : supervision du processus Cubicweb

4.2.2 Formula « asalae »
------------------------

Asalae utilise de nombreux logiciels tiers pour son fonctionnement. Le dossier /srv/salt/sae contient l’ensemble des formules utilisées. L’installation est réalisée avec la formule asalae-install orchestrant l’application des différentes formules.
Dans la dernière version livrée des formulas, les drivers spécifiques au projet SAEM sont automatiquement créés.
A partir de la version SAEM_INSTALL_PACKAGE_V164.tar.gz des formulas SALT installe l’environnement complet d’as@ale en version 1.6.4 avec une base vierge.

1. asalae.init : Détermine s’il agit d’une installation ou d’une mise à jour

4.2.3 Formula « alfresco »
--------------------------

La formule alfresco est accessible depuis le dépôt Git :
https://framagit.org/saemproject/alfresco-formula.git.
Elle est composée de plusieurs « states » :

1. init : Determine s’il agit d’une installation complète ou d’une mise à jour
  a. alfresco : récupération et décompression du ZIP d’installation alfresco sous /opt.
  b. tomcat : installation du serveur tomcat
  c. config : mise à jour des différents fichiers de configuration
  d. clean-deploy : nettoyage des dossiers work, temp et webapps puis redéploiement des WARs.

.. note::
Informations : Les archives (zip et war) nécessaires à alfresco sont assez volumineux (500 Mo) au total. Le transfert de ces fichiers du master au minion peut prendre une dizaine de minutes.

4.3 Installation en Pré-Production
---------------------------------

L’environnement SaltStack par défaut « base » correspond à la pré-production. Les étapes suivantes devront être effectuées à partir de la machine « Salt Master » avec un compte « root ».
1. Editer et adapter les fichiers pillar associés aux trois briques : asal@e, Ged SAS et Référentiel :
  a. /srv/pillar/base/asalae/init.sls
  b. /srv/pillar/base/gedsas/init.sls
  c. /srv/pillar/base/saemref/init.sls
2. Vérifier que le fichier « top.sls » des pillar est configuré suivant les identifiants des minions déclarés sur la plateforme de Pré-Production (fichier /srv/pillar/base/top.sls).
3. Rafraîchir les pillars suivant la commande SaltStack suivante

  $ salt '*' saltutil.refresh_pillar

4. Installer la solution complète via la commande :
----------------------------------
    $ salt '*' state.highstate saltenv=base

.. warning :: Cette commande SaltStack permet d’instancier les trois briques de la solution SAEM suivant la configuration définie au sein des pillar.


5. Si l’installation se déroule avec succès, démarrer depuis le Master Salt les services des différents modules via les commandes SaltStack suivantes :

  $ salt 'referentiel.preprod' cmd.run 'supervisorctl start saemref'
  $ salt 'asalae.preprod' cmd.run 'service httpd start'
  $ salt 'alfresco.preprod' cmd.run 'service alfresco start'

##  4.4 Installation en Production

La procédure à suivre est similaire à l’installation en pré production.

1. Editer et adapter les fichiers pillar associés aux trois briques : Asal@ae, Ged SAS et Référentiel de Production :
  a. /srv/pillar/prod/asalae/init.sls
  b. /srv/pillar/prod/gedsas/init.sls
  c. /srv/pillar/prod/saemref/init.sls
2. Vérifier que le fichier « top.sls » des pillar est configuré suivant les identifiants des minions déclarés sur la plateforme de Production (fichier /srv/pillar/prod/top.sls).
3. Rafraîchir les pillars

  $ salt '*' saltutil.refresh_pillar

4. Installer la solution complète via la commande :
-----------------------------------



.. index:: PhantomJS, Python, SlimerJS

- PhantomJS_ 1.9.1 or greater. Please read the `installation instructions for PhantomJS <http://phantomjs.org/download.html>`_
- Python_ 2.6 or greater for ``casperjs`` in the ``bin/`` directory

.. note::

   CoffeeScript is not natively supported in PhantomJS versions 2.0.0 and above.  If you are going to use CoffeeScript you'll have to transpile it into vanilla Javascript.  See :ref:`known issues <known_issues>` for more details.

.. versionadded:: 1.1

- **Experimental:** as of 1.1.0-beta1, SlimerJS_ 0.8 or greater to run your tests against Gecko (Firefox) instead of Webkit (just add `--engine=slimerjs` to your command line options). The SlimerJS developers documented `the PhantomJS API compatibility of SlimerJS <https://github.com/laurentj/slimerjs/blob/master/API_COMPAT.md>`_ as well as `the differences between PhantomJS and SlimerJS <http://docs.slimerjs.org/current/differences-with-phantomjs.html>`_. Note that it is known that coffescript support breaks as of SlimerJS_ 0.9.6; we are investigating that issue.

.. versionadded:: 1.1.0-beta4

.. warning::

   Versions before 1.1.0-beta4 that were installed through npm required an unspecific PhantomJS version by means of an npm dependency. This led to lots of confusion and issues against CasperJS not working properly if installed through npm. Starting with 1.1.0 the installation of an engine (PhantomJS, SlimerJS) will be a real prerequisite, regardless of the installation method you choose for CasperJS.

.. index:: Homebrew

Installing from Homebrew (OSX)
------------------------------

Installation of both PhantomJS and CasperJS can be achieved using Homebrew_, a popular package manager for Mac OS X.

Above all, don't forget to update Formulaes::

    $ brew update

For the 1.1 development version (recommended)::

    $ brew install casperjs --devel

For the 1.0.x stable version::

    $ brew install casperjs

If you have already installed casperjs and want to have the last release (stable|devel), use ``upgrade``::

    $ brew upgrade casperjs

Upgrade only update to the latest release branch (1.0.x|1.1.0-dev).

Installing from npm
-------------------

.. versionadded:: 1.1.0-beta3

You can install CasperJS using `npm <http://npmjs.org/>`_:

- For most users (current version 1.1.0-beta4):

    $ npm install -g casperjs

- If you want a specific older version:

    - For beta3: $ npm install -g casperjs@1.1.0-beta3

    - For beta2: $ npm install -g casperjs@1.1.0-beta2

- If you want to install the current master from git using npm:

    $ npm install -g git+https://github.com/casperjs/casperjs.git

.. note::

   The ``-g`` flag makes the ``casperjs`` executable available system-wide.

.. warning::

   While CasperJS is installable via npm, :ref:`it is not a NodeJS module <faq_node>` and will not work with NodeJS out of the box. **You cannot load casper by using require('casperjs') in node.** Note that CasperJS is not capable of using a vast majority of NodeJS modules out there. **Experiment and use your best judgement.**

.. index:: git

Installing from git
-------------------

Installation can be achieved using `git <http://git-scm.com/>`_. The code is mainly hosted on `Github <https://github.com/casperjs/casperjs>`_.

From the master branch
~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: text

    $ git clone git://github.com/casperjs/casperjs.git
    $ cd casperjs
    $ ln -sf `pwd`/bin/casperjs /usr/local/bin/casperjs

Once PhantomJS and CasperJS installed on your machine, you should obtain something like this:

.. code-block:: text

    $ phantomjs --version
    1.9.2
    $ casperjs
    CasperJS version 1.1.0-beta4 at /Users/niko/Sites/casperjs, using phantomjs version 1.9.2
    # ...

Or if SlimerJS is your thing:

.. code-block:: text

    $ slimerjs --version
    Innophi SlimerJS 0.8pre, Copyright 2012-2013 Laurent Jouanneau & Innophi
    $ casperjs
    CasperJS version 1.1.0 at /Users/niko/Sites/casperjs, using slimerjs version 0.8.0

You are now ready to write your :doc:`first script <quickstart>`!


Installing from an archive
--------------------------

You can download tagged archives of CasperJS code:

**Latest development version (master branch):**

- https://github.com/casperjs/casperjs/zipball/master (zip)
- https://github.com/casperjs/casperjs/tarball/master (tar.gz)

**Latest stable version:**

- https://github.com/casperjs/casperjs/zipball/1.1.0 (zip)
- https://github.com/casperjs/casperjs/tarball/1.1.0 (tar.gz)

Operations are then the same as with a git checkout.


.. index:: Windows

CasperJS on Windows
-------------------

Phantomjs installation additions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- Append ``";C:\phantomjs"`` to your ``PATH`` environment variable.
- Modify this path appropriately if you installed PhantomJS to a different location.

Casperjs installation additions
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. versionadded:: 1.1.0-beta3

- Append ``";C:\casperjs\bin"`` to your ``PATH`` environment variable (for versions before 1.1.0-beta3 append ``";C:\casperjs\batchbin"`` to your ``PATH`` environment variable).
- Modify this path appropriately if you installed CasperJS to a different location.
- If your computer uses both discrete and integrated graphics you need to disable autoselect and explicitly choose graphics processor - otherwise ``exit()`` will not exit casper.

You can now run any regular casper scripts that way:

``".. code-block:: text

    C:> casperjs myscript.js

Colorized output
~~~~~~~~~~~~~~~~

.. note::

   .. versionadded:: 1.1.0-beta1

   Windows users will get colorized output if ansicon_ is installed or if the user is using ConEmu_ with ANSI colors enabled.

.. index:: Bugs, REPL

Compilation (Optionaly)
~~~~~~~~~~~~~~~~~~~~~~~

- .NET Framework 3.5 or greater (or Mono_ 2.10.8 or greater) for ``casperjs.exe`` in the ``bin/`` directory

Known Bugs & Limitations
------------------------

- Due to its asynchronous nature, CasperJS doesn't work well with `PhantomJS' REPL <http://code.google.com/p/phantomjs/wiki/InteractiveModeREPL>`_.

.. _Homebrew: http://mxcl.github.com/homebrew/
.. _PhantomJS: http://phantomjs.org/
.. _Python: http://python.org/
.. _SlimerJS: http://slimerjs.org/
.. _ansicon: https://github.com/adoxa/ansicon
.. _Mono: http://www.mono-project.com/
.. _ConEmu: https://code.google.com/p/conemu-maximus5/
