.. _referentiel:

.. _referentiel_accueil_module:

Module gestion des données de référence 
---------------------------------------

Fonctionnalités
~~~~~~~~~~~~~~~
Le module offre de produire, importer, gérer et rélier entre elles, puis consulter les données de référence produites par les archivistes.
Ces données sont de plusieurs types :

* les acteurs de l'archivage : autorités administratives (collectivités) se composant d'unités administratives (services versants, producteurs, ou service d'archives), ainsi que les agents qui sont les contacts référents de ces entités. 
* les notices d'autorité : description des producteurs (unités administratives ou producteurs privés) conformément à la norme ISAAR-CPF.
* les vocabulaires contrôlés : listes de termes à plat (liste d'autorité) ou hiérarchisés (thésaurus).
* les profils d'archivage : ensemble des règles définissant les contraintes liées à un versement d'archives.
* les unités d'archives : dossier type, réutilisable au sein d'un profil. Il peut n'être composé que d'une pièce (un fichier) ou d'un ensemble de fichiers et peut lui-même être découpé en sous-dossiers. 

L'intérêt principal du module de gestion des données de référence est de permettre de relier toutes ces entités par des relations typées. On peut ainsi décrire un producteur ou une unité d'archives en utilisant un concept d'un vocabulaire contrôlé ou décrire les relations existante entre des producteurs (relations hiérarchiques, temporelles ou associatives dans les notices d'autorité).

Autres fonctionnalités : 

* Import et export de notices d'autorité au format XML-EAC.
* Import de vocabulaires contrôlés publiés par des institutions de référence comme le SIAF ou la BNF au format SKOS ou de vocabulaires au format CSV.
* Exposition des données suivant le protocole OAI-PMH.

.. _donnees_ref:

.. index:: accueil-donnees-ref

SHERPA
~~~~~~~~~~~~~~~~~~~~~~

Intro à faire.

Notices d'autorité
=========================================

Les notices d'autorités sont des fiches de description des producteurs d'archives, normalisées selon la norme ISAAR-CPF.
Le référentiel implémente le schéma EAC-CPF et permet ainsi d'importer, de créer et d'exporter des notices d'autorités dans un format 
intéropérable avec d'autres applications.

**Pour qui peut-on créer une notice d'autorité ?**

- Une autorité administrative, ce qui permet de décrire la collectivité, ses dates extrêmes, son organisation et ses évolutions.


- Une unité administrative, ce qui permet de détailler l'organisation d'un service producteur, ses missions, ses relations hiérarchiques, chronologiques ou d'association avec d'autres services.


- Un agent d'une autorité administrative : par exemple un directeur de cabinet.


- Une personne physique ou morale, produisant des archives privées électroniques confiées à une service d'archives utilisant la solution SAEM Girondin.



Exemple de la notice d'autorité de la direction de la petite enfance de la ville de Bordeaux. 

On crée une notice d'autorité depuis l'onglet de la page d'accueil.

.. image:: ../images/creation_notice_autorite.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

On peut également créer ou relier une notice d'autorité directement à partir de l'unité administrative à laquelle elle se rapporte.

.. image:: ../images/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center
   

.. image:: ../images/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center


Dans la notice d'autorité, la forme autorisée du nom doit respecter la norme AFNOR NF Z 44-060 (déc.1996) : Catalogage d’auteurs et d'anonymes – forme et structure des vedettes de collectivités
auteurs. On se reportera également au référentiel national des formes autorisées du nom pour l'administration territoriale (1800 à nos jours), produit par le groupe de travail SIAF/AAF.


Présentation des différents onglets des notices d'autorité :


- onglet **informations générales**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_1.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **description**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_2.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **propriétés**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_3.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

- onglet **relations** :


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_4.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

Vocabulaires
====================================

Les vocabulaires sont des listes de termes organisées. Elles peuvent prendre la forme de listes simples (on parle alors de liste d'autorité) ou de listes hiérarchiques (on parle alors de thésaurus).

Il est possible de créer des vocabulaires mais également d'en importer.

- **Créer un vocabulaire directement dans le référentiel :**

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône +. Vous devez donner à ce vocabulaire : 

- un titre : nom du vocabulaire
- une description : qualification du contenu du vocabulaire
- autorité nommante ARK : rattachement à une autorité d'archivage

.. image:: ../images/Doc-SAEM-Crea_Voca_1.png
   :scale: 50%
   :align: center

Une fois créé, le vocabulaire se voit attribué un identifiant unique. Vous pouvez ensuite créer des concepts. Pour ajouter un concept vous pouvez saisir :

- une définition : 
- un exemple d'utilisation
- un ou plusieurs libellés 
- sélectionner un type de libellé (préféré ou alternatif) et un code langue

.. image:: ../images/Doc-SAEM-Crea_Concept_1.png
   :scale: 50%
   :align: center

Une fois le concept créé vous pouvez ajouter un nouveau concept au même niveau ou un sous-concept de celui que vous venez de créér.

.. image:: ../images/Doc-SAEM-Affichage_Concepts.png
   :scale: 50%
   :align: center

- **Créer un vocabulaire en téléchargeant un fichier existant (tableur numérique ou fichier linked csv).**

2 syntaxes sont possibles : 

- format de fichier simple : un tableur numérique (excel ou calc par exemple) ne contenant qu'une colonne
- format de fichier linked csv : un tableur avec une syntaxe particulière permettant l'import de thésaurus hiérarchiques.

Une section de la documentation détaille plus précisément la syntaxe attendue. Voir `mon fichier <https://framagit.org/saemproject/saemdoc/raw/master/source/import_lcsv_skos_complet.pdf>`_


- **Importer un vocabulaire skos présent sur le web :**

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône importer.

.. image:: ../images/Doc-SAEM-Import_Voca_Skos_1.png
   :scale: 50%
   :align: center
   
 Exemple d'affichage d'un thésaurus :
   
.. image:: ../images/Doc-SAEM-Thesaurus.PNG
   :scale: 50%
   :align: center   

A ce stade les vocabulaires créés ou importés sont en mode brouillon. Pour devenir accessibles aux autres modules, il est nécessaire de les publier. Lorsqu'ils le sont, il est toujours possible de rajouter des termes ou de les modifier mais plus de les supprimer.


.. index:: seda_profile

Profils SEDA
===================================


Reprise de la documentation

Rôle du profil et des unités d'archives

Pour préparer des versements automatisés ou réguliers d'un flux d'archives électroniques, l'étude du flux permet de définir un plan de classement type, ainsi que le contenu attendu avec une normalisation des intitulés et la définition de règles de gestion qui s'appliqueront à ces archives : DUA, sort final ou communicabilité par exemple. L'ensemble des règles définies constitue un profil SEDA.
L'utilisation d'un profil est particulièrement valable pour les productions de type sériel (les dossiers annuels de versement d'une typologie documentaire, les arrêtés, les actes, les dossiers de marchés, etc...). 

Sur la page d'accueil du référentiel il existe un onglet **Profils SEDA** et un onglet **Unités d'archives**.
L'onglet **Unités d'archives** permet de créer des unités d'archives par exemple pour décrire et donner des règles à des dossiers types réutilisables dans différents contextes. 
A chaque unité d'archives on peut associer un ou plusieus objets-données (c'est à dire des documents/fichiers).
Un profil est constitué d'une unite d'archive chapeau composée elle-même d'une ou de plusieurs unités d'archives constituant le plan de classement attendu du versement : ces unités d'archives sont l'équivalent de sous-dossiers. Elles peuvent être soient créées directement dans le profil soient importées (réutilisation d'une unité d'archive déjà créée).


Une fois qu'il est créé, un profil est associé à une ou plusieurs unités administratives de type service versant qui pourront l'utiliser dans le module gestion des processus d'archivage pour faire des versements. 
Les renseignements indiqués dans le profil, ainsi que ceux qui seront ajoutés ou captés via les métadonnées sur les archives lors du transfert constitueront le bordereau de versement.

Un même profil peut être utilisé par différentes collectivités ou différents services, s'il concerne des types de documents semblables comme c'est le cas des pièces de passation d'un marché public par exemple.

Création de profil et d'unité d'archives pas à pas 

Par exemple, dans une collectivité ayant la compétence voirie, infrastructures de déplacement ou encore aménagement urbain, on peut envisager de créer un profil de versement pour les opérations d'aménagement de voirie.

Ce profil utilisera des dossiers types suivants : 

* dossier maîtrise d'ouvrage : études pré opérationnelles, actes réglementaire de la collectivité, étude de faisabilité 
* dossier maîtrise d'oeuvre : étude préliminaire, avant projet sommaire, avant projet détaillé
* dossiers d'interventions ultérieures sur l'ouvrage : prescriptions techniques d'entretie
* dossier de remise en gestion de l'ouvrage : plan de récolement, procès-verbaux
 
Ces types de dossiers peuvent être crées comme des unités d'archives, avec leur organisation en sous-dossier et leurs fichiers type.
Ces unités d'archives peuvent être utilisées comme des sous éléments d'autres profils le cas échéant : par exmple un profil pour le versement d'archives liées à un ouvrage d'art qui utilisera notamment les unités d'archives dossier mâitrise douvrage et dossier d'interventions ultérieurs sur l'ouvrage.



- Créer un profil SEDA :

Après avoir donné un nom au profil, il faut constituer une unité d'archives chapeau, de niveau supérieur qui servira d'enveloppe pour réunir toutes les autres unités d'archives (et leurs fichiers) en un seul et même versement.
La cardinalité de cette première unité d'archive doit être 1 ou 1-n. 

Le niveau de description et la règle de restriction d'accès de cette unité d'archives doivent être renseignés. Si elles ne le sont pas dans le profil, ces informations devront être complétées au moment du versement. 
Par ailleurs, on peut écrire le nom de l'unité d'archives directement ou le faire compléter dans le formulaire au moment de la préparation du versement, en précisant comment formuler l'intitulé dans le commentaire d'aide à la saisie. A VERIFIER dans GED SAS



**A VERIFIER PAR TESTS** 
Au niveau inférieur les **unités d'archives** correspondent aux dossiers et sous-dossiers types d'un versement.

Sous les unités d'archives on trouve les **objets-données**. 


Un objet-donnée est une description d'un document attendu dans le versement (type, format, dates etc.).


- **Exemple d'un profil pour une versement type d'un dossier de recrutement.**
*On voit à droite l'arborescence prédéfinie du profil, avec les sous dossiers qu'on s'attend à trouver dans chaque versement et, quand il sont connus à l'avance,
les fichiers indispensables ou facultatifs dans chaque sous-dossier.*

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement.PNG


- **Exemple de saisie d'informations au niveau du premier sous-dossier unité d'archives.**

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement-UD1.PNG



**Comment construire un profil dans le référentiel ?**


On part du principe qu'un document type peut être décrit de la même manière même si il est utilisé dans des contextes différents.

Exemples : 

- une "lettre de motivation" peut être présente dans un dossier "candidature" en réponse à offre de recrutement ou dans un dossier "formation" pour solliciter une autorisation

- un "ordre de mission" peut être présent dans un sous-dossier "déplacement professionnel" ou dans un dossier "formation".


De même on considère qu'un dossier ou un sous-dossier type peut être utilisé dans différents profils.

Exemples :

- un dossier type "déplacement professionnel" peut être un élément du "dossier administratif" d'un agent ou d'un dossier "colloque"
et ainsi de suite.

On part donc du plus petit dénominateur commun pour définir des entités objets-donnés, unités d'archives ou profils les plus génériques et réutilisables possibles.


En vue de la création du profil "Dossier de recrutement d'un agent dans la collectivite" on a d'abord créé des objets données individuels.

*Nota bene :* 

- pour avoir un affichage du nom type de l'objet-données (par exemple "curriculum vitae") dans l'arborescence du profil, il faut l'écrire en dur dans le champ "commentaire d'aide à la saisie" de la cardinalit de l'objet-données car cette balise nom type n'existe pas dans le SEDA.

- pour avoir plus tard l'affichage du nom type de l'objet-données dans le formulaire de saisie du module gestion des processus d'archivage (afin de guider l'utilisateur au moment des versements pour qu'il sache quel type de document associer au bon endroit), il faut l'écrire en dur dans le champ "description".


**Exemple de constitution d'un objet donnée réutilisable, exemple d'un Curriculum vitae**

.. image:: ../images/Doc-SAEM-REF-OngletUnitesArchives-ExObjetDonneeCV.PNG


Ensuite on crée de la même manière des unités d'archives qui vont constituer des sous dossiers type d'un dossier de recrutement.

Par exemple on crée l'unité **"Réponse à une offre d'emploi et pièces justificatives"**

A l'intérieur on peut **importer** des objets données nécessaires existants ou en **ajouter** en indiquant la cardinalité :

- 1 si l'objet est obligatoire et unique (1 cv ou une lettre de motication par exemple)

- 1-n si l'objet est obligatoire et peut être multiple (des pièces justificatives liées à la candidature)

- 0-1 si l'objet est facultatif et unique

- 0-n si l'objet est facultatif et peut être multiple (des lettres de rcommandation par exemple)


Enfin on crée de la même manière un profil en important des unités d'archives prédéfinies dont on peut le cas échant, avant publication du profil, modifier les propriétés (cardinalités ou règles de gestion par exemple).

Cette méthode permet de réutiliser des entités déjà définies dans un nouveau contexte, favorise la normalisation des versements et permet un travail collaboratif entre archivistes.

- **Association d'un profil à une unité administrative via le bouton ajouter**
.. image:: ../images/Doc-SAEM-Association-Profil-SV.PNG

Le paragraphe suivant a été déplacé de la partie GED SAS (validité à vérifier et intégrer aux bons endroits si nécessaire)

Règles de création de profil SEDA pour le projet SAEM
Les normes SEDA offrent de nombreuses libertés à la création des profils. Nous fixons quelques règles ici afin de permettre de créer des profils SEDA optimisé pour le projet SAEM .
Le profil SEDA créé doit :
-	toujours demander un commentaire en saisie libre (de cardinalité 1)
-	les UA de plus haut niveau doivent également être en saisie libre car elles sont reprises par as@lae lors de la transformation d’un transfert en entrée.  
-	Toute UA doit disposer parmi ses descendants d’un document afin de déclencher le calcul des différentes dates.
-	Tout descripteur contrôlé doit être associé à un vocabulaire et à un type de mot clés.

* objet donnée (A REPLACER AU BON ENDROIT°
 Il est possible de définir sur un profil les types mimes et les formats souhaités pour des documents. Pour cela il suffit de renseigner le champ "categorie de fichier.

Il sera proposé des valeurs provenant du Vocabulaire (catégorie de fichier), qui est de la forme suivante : 

--Extension

----Liste de type mimes associés

-------Liste de formats associés

**Règle d'implémentation des noms des versements (copie du paragraphe intégré dans le module GED SAS : voir si et comment on le reprend)**

Cas 1 : Dans le module Référentiel, le champs commentaire est renseigné avec une valeur définie. Il est non modifiable dans le formulaire de la GED SAS et envoyé sans modification vers as@lae. La valeur est associés à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco.

NB : c’est la valeur de cette nouvelle métadonnée qui sera systématiquement repris dans la dashlet « Mes traitements ».

Cas 2 : Dans le module Référentiel, le champs commentaire est obligatoire avec une valeur libre (cardinalité 1). Le champs est donc modifiable et obligatoire dans le formulaire de la GED et la valeur saisie est envoyée vers as@lae. Là encore, la valeur saisie est associée à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco.

Cas 3 : Dans le module Référentiel, le champs commentaire n’est pas demandé. Le champs n’existe pas dans le formulaire. Un nom système « Versement » associé à un timestamp est créé. Aucune balise sur le commentaire n'est transmise à as@lae.

Cas 4 : Dans le module Référentiel, le champs commentaire est demandé avec une valeur libre avec une cardinalité (0-1). Le champs est donc modifiable dans le formulaire de la GED SAS. Si la valeur est saisie, le comportement du cas 2 s'applique. Si la valeur n’est pas saisie, le comportement du cas 3 s'applique. 

   
Données de référence
~~~~~~~~~~~~~~~~~~~~~~

Le module de gestion des données de référence contient plusieurs types de données reliées entre elles par une ontologie.
   
index:: organization_unit

Les acteurs de l'archivage
================================================

Les autorités administratives et leurs entités associées (unités administratives et agents) sont des acteurs du système d'archivage électronique
au sens du SEDA. Les unités administratives notamment se voient attibuer un rôle archivistique comme service versant, service producteur, service d'archives.

Lorsque les modules du SAEM sont synchronisés, ces unités administratives et leurs contacts référents sont automatiquement déclarés comme intervenants et utilisateurs dans les modules gestion des processus d'archivage 
et gestion de l'archivage et de la conservation.

Ces entités qui pré existent dans chaque collectivité en dehors de l'archivage électronique, sont déjà partiellement décrites dans des outils de gestion des ressources humaines (organigrammes, annuaires LDAP ou autres applications du sytème d'information). Ces outils internes pourront à terme créer automatiquement des unités administratives et des agents.

Au sein du référentiel, une **autorité administrative** est une collectivité (commune, département ou un établissement public) "mère", 
composée de plusieurs **unités administratives** "filles" (directions ou services) et de x **agents** de type personne (employés). 
L'autorité administrative n'a pas de rôle archivistique propre ; les rôles sont définis pour les unités.

Il est obligatoire d'associer un service d'archives à une autorité administrative lorsqu'on la crée. 
Par héritage, toutes les unités administratives de cette autorité auront le même service d'archives qui est logiquement compétent pour toute la collectivité.

- Créer une autorité administrative ville de Bordeaux.

.. image:: ../images/Doc-SAEM-Crea_Autorite_Admin_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center
  
  
   
.. image:: ../images/Doc-SAEM-Crea_Autorite_Admin_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

On dispose maintenant d'une autorité administrative, au sein de laquelle on peut créer des unités administratives "filles".

.. image:: ../images/Doc-SAEM-Crea_Unite_Admin_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer le service d'archives compétent pour toute la ville. Il s'agit d'une unité administrative de la ville de Bordeaux.

.. image:: ../images/Doc-SAEM-Crea_Unite_Admin_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer un contact pour le service d'archives, puis le publier. Cet agent sera le contact référent du service d'archives.

.. image:: ../images/Doc-SAEM-Crea_Agent_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

.. image:: ../images/Doc-SAEM-Crea_Agent_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Une fois publié, l'agent créé apparaît dans une liste déroulante des contacts référents possibles du service d'archives, ce qui permet de le sélectionner. Puis, publier le service d'archives.

.. image:: ../images/Doc-SAEM-Asso_Agent_UA.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Associer le service d'archives compétent à l'autorité administrative.

.. image:: ../images/Doc-SAEM-Asso_ServiceArchives_AutoriteAdministrative_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

- Créer les autres unités administratives "filles", pour les services versants. Pour chacun d'entre eux, créer un agent, le publier et l'associer au service versant en tant que contact référent.

.. image:: ../images/Doc-SAEM-Crea_Unite_Admin_3.png
   :scale: 50%
   :alt: capture d'écran de la page de paramétrage de l'unité administrative
   :align: center

Les acteurs de l'archivage sont maintenant créés.

.. index:: authority_record

Notices d'autorité
=========================================

Les notices d'autorités sont des fiches de description des producteurs d'archives, normalisées selon la norme ISAAR-CPF.
Le référentiel implémente le schéma EAC-CPF et permet ainsi d'importer, de créer et d'exporter des notices d'autorités dans un format 
intéropérable avec d'autres applications.

Dans le référentiel, chaque notice d'autorité présente des informations sur un producteur qui sont classée dans quatre permiers onglets (informations générales, description, propriétés, relations). Ces onglets correspondent globalement aux zones de la norme ISAAR-CPF et présentent les balises/champs à compléter ou ajouter.
Un cinquième onglet "cycle de vie" correspond à l'enregistrement des évenements effectués sur la notice.

**Pour qui peut-on créer une notice d'autorité ?**

- Une autorité administrative, ce qui permet de décrire la collectivité, ses dates extrêmes, son organisation et ses évolutions.


- Une unité administrative, ce qui permet de détailler l'organisation d'un service producteur, ses missions, ses relations hiérarchiques, chronologiques ou d'association avec d'autres services.


- Un agent d'une autorité administrative : par exemple un directeur de cabinet.


- Une personne physique ou morale, produisant des archives privées électroniques confiées à une service d'archives utilisant la solution SAEM Girondin.



Exemple de la notice d'autorité de la direction de la petite enfance de la ville de Bordeaux. 

On crée une notice d'autorité depuis l'onglet de la page d'accueil.

.. image:: ../images/Doc-SAEM-Crea_NoticeAutorite_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

On peut également créer ou relier une notice d'autorité directement à partir de l'unité administrative à laquelle elle se rapporte.

.. image:: ../images/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_1.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center
   

.. image:: ../images/Doc-SAEM-Crea_NoticeAutorite_DepuisUA_2.png
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center


Dans la notice d'autorité, la forme autorisée du nom doit respecter la norme AFNOR NF Z 44-060 (déc.1996) : Catalogage d’auteurs et d'anonymes – forme et structure des vedettes de collectivités
auteurs. On se reportera également au référentiel national des formes autorisées du nom pour l'administration territoriale (1800 à nos jours), produit par le groupe de travail SIAF/AAF.


Présentation des différents onglets des notices d'autorité :


- onglet **informations générales**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_1.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **description**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_2.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **propriétés**:


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_3.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

- onglet **relations** :


.. image:: ../images/Doc-SAEM-Exemp_NoticeAutorite_4.png
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


.. index:: vocabulaires

Vocabulaires
====================================

Les vocabulaires sont des listes de termes organisées. Elles peuvent prendre la forme de listes simples (on parle alors de liste d'autorité) ou de listes hiérarchiques (on parle alors de thésaurus).

Il est possible de créer des vocabulaires mais également d'en importer.

- **Créer un vocabulaire directement dans le référentiel :**

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône +. Vous devez donner à ce vocabulaire : 

- un titre : nom du vocabulaire
- une description : qualification du contenu du vocabulaire
- autorité nommante ARK : rattachement à une autorité d'archivage

.. image:: ../images/Doc-SAEM-Crea_Voca_1.png
   :scale: 50%
   :align: center

Une fois créé, le vocabulaire se voit attribué un identifiant unique. Vous pouvez ensuite créer des concepts. Pour ajouter un concept vous pouvez saisir :

- une définition : 
- un exemple d'utilisation
- un ou plusieurs libellés 
- sélectionner un type de libellé (préféré ou alternatif) et un code langue

.. image:: ../images/Doc-SAEM-Crea_Concept_1.png
   :scale: 50%
   :align: center

Une fois le concept créé vous pouvez ajouter un nouveau concept au même niveau ou un sous-concept de celui que vous venez de créér.

.. image:: ../images/Doc-SAEM-Affichage_Concepts.png
   :scale: 50%
   :align: center

- **Créer un vocabulaire en téléchargeant un fichier existant (tableur numérique ou fichier linked csv).**

2 syntaxes sont possibles : 

- format de fichier simple : un tableur numérique (excel ou calc par exemple) ne contenant qu'une colonne
- format de fichier linked csv : un tableur avec une syntaxe particulière permettant l'import de thésaurus hiérarchiques.

Une section de la documentation détaille plus précisément la syntaxe attendue. Voir `mon fichier <https://framagit.org/saemproject/saemdoc/raw/master/source/import_lcsv_skos_complet.pdf>`_


- **Importer un vocabulaire skos présent sur le web :**

Aller sur la dashlet ou l'onglet vocabulaire et cliquer sur l'icône importer.

.. image:: ../images/Doc-SAEM-Import_Voca_Skos_1.png
   :scale: 50%
   :align: center
   
 Exemple d'affichage d'un thésaurus :
   
.. image:: ../images/Doc-SAEM-Thesaurus.PNG
   :scale: 50%
   :align: center   

A ce stade les vocabulaires créés ou importés sont en mode brouillon. Pour devenir accessibles aux autres modules, il est nécessaire de les publier. Lorsqu'ils le sont, il est toujours possible de rajouter des termes ou de les modifier mais plus de les supprimer.


.. index:: seda_profile

Profils SEDA
===================================


Reprise de la documentation

Rôle du profil et des unités d'archives

Pour préparer des versements automatisés ou réguliers d'un flux d'archives électroniques, l'étude du flux permet de définir un plan de classement type, ainsi que le contenu attendu avec une normalisation des intitulés et la définition de règles de gestion qui s'appliqueront à ces archives : DUA, sort final ou communicabilité par exemple. L'ensemble des règles définies constitue un profil SEDA.
L'utilisation d'un profil est particulièrement valable pour les productions de type sériel (les dossiers annuels de versement d'une typologie documentaire, les arrêtés, les actes, les dossiers de marchés, etc...). 

Sur la page d'accueil du référentiel il existe un onglet **Profils SEDA** et un onglet **Unités d'archives**.
L'onglet **Unités d'archives** permet de créer des unités d'archives par exemple pour décrire et donner des règles à des dossiers types réutilisables dans différents contextes. 
A chaque unité d'archives on peut associer un ou plusieus objets-données (c'est à dire des documents/fichiers).
Un profil est constitué d'une unite d'archive chapeau composée elle-même d'une ou de plusieurs unités d'archives constituant le plan de classement attendu du versement : ces unités d'archives sont l'équivalent de sous-dossiers. Elles peuvent être soient créées directement dans le profil soient importées (réutilisation d'une unité d'archive déjà créée).


Une fois qu'il est créé, un profil est associé à une ou plusieurs unités administratives de type service versant qui pourront l'utiliser dans le module gestion des processus d'archivage pour faire des versements. 
Les renseignements indiqués dans le profil, ainsi que ceux qui seront ajoutés ou captés via les métadonnées sur les archives lors du transfert constitueront le bordereau de versement.

Un même profil peut être utilisé par différentes collectivités ou différents services, s'il concerne des types de documents semblables comme c'est le cas des pièces de passation d'un marché public par exemple.

Création de profil et d'unité d'archives pas à pas 

Par exemple, dans une collectivité ayant la compétence voirie, infrastructures de déplacement ou encore aménagement urbain, on peut envisager de créer un profil de versement pour les opérations d'aménagement de voirie.

Ce profil utilisera des dossiers types suivants : 

* dossier maîtrise d'ouvrage : études pré opérationnelles, actes réglementaire de la collectivité, étude de faisabilité 
* dossier maîtrise d'oeuvre : étude préliminaire, avant projet sommaire, avant projet détaillé
* dossiers d'interventions ultérieures sur l'ouvrage : prescriptions techniques d'entretie
* dossier de remise en gestion de l'ouvrage : plan de récolement, procès-verbaux
 
Ces types de dossiers peuvent être crées comme des unités d'archives, avec leur organisation en sous-dossier et leurs fichiers type.
Ces unités d'archives peuvent être utilisées comme des sous éléments d'autres profils le cas échéant : par exmple un profil pour le versement d'archives liées à un ouvrage d'art qui utilisera notamment les unités d'archives dossier mâitrise douvrage et dossier d'interventions ultérieurs sur l'ouvrage.



- Créer un profil SEDA :

Après avoir donné un nom au profil, il faut constituer une unité d'archives chapeau, de niveau supérieur qui servira d'enveloppe pour réunir toutes les autres unités d'archives (et leurs fichiers) en un seul et même versement.
La cardinalité de cette première unité d'archive doit être 1 ou 1-n. 

Le niveau de description et la règle de restriction d'accès de cette unité d'archives doivent être renseignés. Si elles ne le sont pas dans le profil, ces informations devront être complétées au moment du versement. 
Par ailleurs, on peut écrire le nom de l'unité d'archives directement ou le faire compléter dans le formulaire au moment de la préparation du versement, en précisant comment formuler l'intitulé dans le commentaire d'aide à la saisie. A VERIFIER dans GED SAS



**A VERIFIER PAR TESTS** 
Au niveau inférieur les **unités d'archives** correspondent aux dossiers et sous-dossiers types d'un versement.

Sous les unités d'archives on trouve les **objets-données**. 


Un objet-donnée est une description d'un document attendu dans le versement (type, format, dates etc.).


- **Exemple d'un profil pour une versement type d'un dossier de recrutement.**
*On voit à droite l'arborescence prédéfinie du profil, avec les sous dossiers qu'on s'attend à trouver dans chaque versement et, quand il sont connus à l'avance,
les fichiers indispensables ou facultatifs dans chaque sous-dossier.*

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement.PNG


- **Exemple de saisie d'informations au niveau du premier sous-dossier unité d'archives.**

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement-UD1.PNG



**Comment construire un profil dans le référentiel ?**


On part du principe qu'un document type peut être décrit de la même manière même si il est utilisé dans des contextes différents.

Exemples : 

- une "lettre de motivation" peut être présente dans un dossier "candidature" en réponse à offre de recrutement ou dans un dossier "formation" pour solliciter une autorisation

- un "ordre de mission" peut être présent dans un sous-dossier "déplacement professionnel" ou dans un dossier "formation".


De même on considère qu'un dossier ou un sous-dossier type peut être utilisé dans différents profils.

Exemples :

- un dossier type "déplacement professionnel" peut être un élément du "dossier administratif" d'un agent ou d'un dossier "colloque"
et ainsi de suite.

On part donc du plus petit dénominateur commun pour définir des entités objets-donnés, unités d'archives ou profils les plus génériques et réutilisables possibles.


En vue de la création du profil "Dossier de recrutement d'un agent dans la collectivite" on a d'abord créé des objets données individuels.

*Nota bene :* 

- pour avoir un affichage du nom type de l'objet-données (par exemple "curriculum vitae") dans l'arborescence du profil, il faut l'écrire en dur dans le champ "commentaire d'aide à la saisie" de la cardinalit de l'objet-données car cette balise nom type n'existe pas dans le SEDA.

- pour avoir plus tard l'affichage du nom type de l'objet-données dans le formulaire de saisie du module gestion des processus d'archivage (afin de guider l'utilisateur au moment des versements pour qu'il sache quel type de document associer au bon endroit), il faut l'écrire en dur dans le champ "description".


**Exemple de constitution d'un objet donnée réutilisable, exemple d'un Curriculum vitae**

.. image:: ../images/Doc-SAEM-REF-OngletUnitesArchives-ExObjetDonneeCV.PNG


Ensuite on crée de la même manière des unités d'archives qui vont constituer des sous dossiers type d'un dossier de recrutement.

Par exemple on crée l'unité **"Réponse à une offre d'emploi et pièces justificatives"**

A l'intérieur on peut **importer** des objets données nécessaires existants ou en **ajouter** en indiquant la cardinalité :

- 1 si l'objet est obligatoire et unique (1 cv ou une lettre de motication par exemple)

- 1-n si l'objet est obligatoire et peut être multiple (des pièces justificatives liées à la candidature)

- 0-1 si l'objet est facultatif et unique

- 0-n si l'objet est facultatif et peut être multiple (des lettres de rcommandation par exemple)


Enfin on crée de la même manière un profil en important des unités d'archives prédéfinies dont on peut le cas échant, avant publication du profil, modifier les propriétés (cardinalités ou règles de gestion par exemple).

Cette méthode permet de réutiliser des entités déjà définies dans un nouveau contexte, favorise la normalisation des versements et permet un travail collaboratif entre archivistes.

- **Association d'un profil à une unité administrative via le bouton ajouter**
.. image:: ../images/Doc-SAEM-Association-Profil-SV.PNG

Données de référence : unités d'archives ou objets données 
============================================================

fréquemment la production administrative est regroupée au sein de dossiers car on utilise par analogie l'unité intellectuelle que l'on traite et le support au sein de laquelle on la c
Ces unités d'archives sont regroupées au sein de profil d'archivage pour systématiser les règles d'archivage qui leur sont appliquées dans le cadre de productions sérielles (les arrêtés, les actes, les dossiers de marchés, etc...)

Actuellement, sur la page d'accueil du référentiel il existe un onglet **Profils SEDA** et un onglet **Unités d'archives**.
L'onglet **Unités d'archives** permet de creer soit des unités d'archive, soit des objets données. 

.. image:: ../images/Doc-SAEM-REF-OngletUnitesArchivesCreation.PNG

Concernant l'utilisation des unités d'archives ou des objets données, voir les explications ci-dessus dans la section Données de référenc : Profils SEDA


Attribution d'identifiants aux données de référence : les autorités nommantes ark
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Un ark est un identifiant perenne d'une ressource ou d'un objet disponible sur le web, quel que soit sa nature et indépendamment
de modifications du site sur lequel elle/il apparait. 

L'ark se compose de plusieurs parties :
un préfixe correspondant à l'autorité nommante, 
une ou plusieurs lettres permettant de qualifier les ressources selon leur type (r - record - pour notices d'autorité, o pour organisation, ou - organization unit - pour unités administratives, oa pour les agent v pour vocabulaires, c pour concepts créé dans le référentiel, p pour profils, et a pour archives - dossier ou fichiers - ) 
un numéro incrémental.
C'est l'adresse de la ressource sur le net.

Au sein du référentiel, les autorités nommantes sont des instititutions habilitées à attribuer des ark aux différentes entités 
Elles sont déclarées par d'administrateur du référentiel dans l'onglet **Administration** de la page d'accueil du référentiel.
Le référentiel attibue des identifiants ark aux entités crées dans le référentiel : notices d'autorités, unités administratives, vocabulaires, concept, profils.
Le référentiel permet également d'attribuer des identifiants ark aux archives (dossiers ou fichiers) dans le module de gestion des processus d'archivage (ark correspondant à l'identifiant de l'objet d'archive donné par le service versant) et dans le module gestion de l'archivage et de la conservation (ark correspondant à la cote de l'objet).


**Exemple de la création de l'autorité nommante Ville de Saint-Etienne, qui porte le code 23578 (préfixe)**.


- Onglet **Administration**


.. image:: ../images/Doc-SAEM-REF-OngletAdministration.PNG


- Sous menu **Autorités nommantes Ark**


.. image:: ../images/Doc-SAEM-REF-OngletAdministration-AutoriteNommanteArk.PNG


- **Exemple de Saint-Etienne**


.. image:: ../images/Doc-SAEM-REF-OngletAdministration-AutoriteNommanteArk-ExVilleStEtienne.PNG

En associant à une autorité nommante définie: 

- des autorités administratives et leurs unités administratives et notices d'autorités, 

- des vocabulaires,

- ou des profils d'archivage, 
on permet que toutes ces entités "données de référence" soient identifiées avec un numéro ark unique qui commence par le préfixe de l'autorité nommante choisie, identifiant ainsi la collectivité qui a crée la notice.

Par exemple un profil d'archivage qui porte le numéro ark 23578/p000198340 est composé du préfixe identifiant son autorité nommante (23578 pour Saint-Etienne) 
+ de la lettre p pour profil + du numéro incrémental attribué automatiquement par le référentiel 000198340.

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~
todo

Ergonomie, navigation et recherche dans le module gestion des données de référence : généralités
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La Page d'accueil du référentiel présente plusieurs onglets.

L'onglet **Administration** réservé aux administrateurs permet de : 

- déclarer des autorités nommantes

- gérer les utilisateurs et les groupes

- importer des vocabulaires au format SKOS présents sur le web

- visualiser le modèle de donnée de l'applicatif (complet, par type d'entité ou type de relations)


L'onglet **Autorités administratives** permet de déclarer les acteurs de l'archivage manuellement ou dans le futur par connexion avec les annuaires des collectivités.


Les onglets **Vocabulaire**,  **Notices d'autorité** et **Profils SEDA** + **Unités d'archives** qui seront bientôt regroupés définissent des types dentités
qui permettent de qualifier les producteurs d'archives, de définir les termes et règles d'indexation ainsi que les contraintes d'archivage par flux.


Pour les différents onglets l'utilisateur dispose de boutons :

- vue (oeil)

- création (+) 

- ou import le cas échéant


En haut à droite une fenêtre de recherche type Google permet de recherche un contenu dans toutes les types d'entités.


A l'intérieur des entités/onglet d'autres fenêtres de recherche sont disponibles.
Les résultats de recherche sont présentés sous forme de facettes.

En haut au milieu un fil d'arianne précise où est l'utilisateur.

Chaque entité ou sous entité a une adresse dans le logiciel.


Des identifiants ark sont par ailleurs attribués aux
 
- vocabulaires
 
- concepts des vocabulaires 
 
- notices d'autorité
 
- profils SEDA
 
- autorités administratives

- unités administrative 

- agents

Pour les entités Vocabulaires et Notices d'autorité le **cycle de vie** enregistre tous les évènements de l'entité (création, modification, publication, suppression le cas échéant).


Pour l'entité profil des statuts ont été créés : brouillon, publié, déprécié.


Sur la page de chaque entité en haut à gauche un onglet **action** permet d'agir selon son profil sur l'entité.



.. _cubicweb: http://cubicweb.org/project/cubicweb-saem_ref


.. _référentiel national des formes autorisées du nom pour l'administration territoriale (1800 à nos jours): http://archivistes.org/Notices-d-autorite-producteurs-1781



