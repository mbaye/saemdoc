.. _gedsas:

.. index:: accueil-module

Module gestion des processus d'archivage
---------------------------------------
Ce module est accessible par les services versants et les archivistes. C'est un sas d'échange pour la réalisation des processus d'archivage.

Fonctionnalités
~~~~~~~~~~~~~~~
Ce module est le lieu d'échange entre les services versants et les services archives. Il permet de réaliser les processus liés aux archives (versements, éliminations, communications, restitutions) et d'organiser les versements des services.

La plupart des données qui permettent au module de fonctionner sont issues du référentiel. Celui-ci permet en effet de créer des sites collaboratifs, des catégories et des dossiers dits "profilables" dans les espaces documentaires des services versants automatiquement.
Une action visible depuis chaque site collaboratif Share et depuis le tableau de bord des administrateurs Alfresco permet de lancer manuellement la synchronisation sans attendre l'exécution de la tâche planifiée. Cette synchronisation est assurée par des webservices  respectant le protocole OAI-PMH. 

Lancée depuis le tableau de bord des administrateurs Alfresco, la synchronisation est dite globale.
Lancée depuis un site collaboratif, la synchronisation est dite spécifique.

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~

Créer des utilisateurs GED SAS
====================================

-	Se connecter en tant qu'administrateur

.. image:: ../images/creationUtilisateurEtape1.png
   :height: 252px
   :width: 544px
   :scale: 60%
   :alt: écran membre site
   :align: center
 
 
-	Accéder à l’interface des utilisateurs
-	Choisir le menu « Nouvel utilisateur »
 
 
.. image:: ../images/creationUtilisateurEtape2.png
   :height: 252px
   :width: 544px
   :scale: 60%
   :alt: écran membre site
   :align: center

-	Saisir à minima les valeurs obligatoires distinguées par un astérisque en veillant à bien respecter l'orthographe du nom saisi dans le référentiel
-	Valider en fonction des actions par le bouton « Créer un utilisateur » ou « Créer, puis en créer un autre »

 
.. image:: ../images/creationUtilisateurEtape3.png
   :height: 252px
   :width: 544px
   :scale: 60%
   :alt: écran membre site
   :align: center


Synchroniser avec le référentiel
==============================

La synchronisation des données du Référentiel avec la GED SAS est assurée par une tâche planifiée à paramétrer sur le serveur de la GED SAS. Sa périodicité est définie via la propriété « synchro.cronExpression » mentionné dans le fichier de propriétés « saem.properties » du serveur.

Lancer une "synchro Référentiel" pour exécuter manuellement le processus de synchronisation avec le module référentiel. 

.. image:: ../images/synchroSite_GEDSAS.png
   :height: 197px
   :width: 870px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

Ce processus ne peut être exécuté de manière simultanée. Si une demande de synchronisation est effectuée alors qu’une autre est déjà en cours de traitement, un message prévient l’utilisateur de cette information :

.. image:: ../images/synchroEnCours.png
   :height: 108px
   :width: 496px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

La GED SAS importe sous forme de sites collaboratifs Share les unités administratives (services ou directions) publiées dans le référentiel. Elle les distingue en fonction du rôle archivistique qui leur a été attribué par le référentiel :

* Unités administratives ayant le rôle « service versant » (qui ont un thème de couleur verte),
* Unités administratives ayant le rôle « service archive » (qui peuvent aussi avoir un rôle de service versant, et qui ont une thème de couleur violette).

Un site versant possède les caractéristiques suivantes :

* Un tableau de bord normalisé intégrant les dashlets spécifiques « Mes actions » et « Mes traitements »:

.. image:: ../images/dashboardSiteVersant.png
   :height: 620px
   :width: 1042px
   :scale: 80%
   :alt: écran tableau de bord
   :align: center

* Un gestionnaire correspondant au « contact référent » qui lui est associé dans le module Référentiel. 

Un site archives possède les caractéristiques suivantes :

* Un tableau de bord normalisé intégrant les dashlets spécifiques « Mes actions » et « Mes traitements »
* Une dashlet supplémentaire « Mes sites versants » qui permet de lister l’ensemble des sites versants associés au site archive.

.. image:: ../images/dashletMesSitesVersants.png
   :height: 298px
   :width: 572px
   :scale: 80%
   :alt: écran dashlet Mes services versants
   :align: center

Préparer le site service archives
=================================

* Se connecter dans la GED SAS avec le compte utilisateur créé pour le gestionnaire du site archives (contact référent associé dans le référentiel)
* Aller dans mes tâches et accepter l'invitation à rejoindre le site
* Dans le site archives, indiquer l'accord de versement : 

    1. Cliquer sur l'engrenage en haut à droite
    
    2. Modifier les détails du site
    
    3. Compléter le nom de l'identifiant de l'accord de versement paramétré dans As@lae

**Inviter un nouvel archiviste sur un site d'archive**


* Cliquer sur le bouton "Inviter des utilisateurs" dans le dashboard principal du site (bouton silhouette en haut à droite)

 .. image:: ../images/invitation_utilisateur_1.png
   :alt: image choix utilisateur
   :align: center

* Choisir un utilisateur interne (déja créé dans la GED SAS) ou inviter un utilisateur externe :
 
 .. image:: ../images/invitation_utilisateur_2.png
   :height: 147px
   :width: 447px
   :scale: 80%
   :alt: image choix utilisateur
   :align: center
  
 
* Lui attribuer le rôle de gestionnaire du site.

 
 .. image:: ../images/invitation_utilisateur_3.png
   :height: 147px
   :width: 447px
   :scale: 80%
   :alt: image choix du role
   :align: center
   
   
* valider

  
 .. image:: ../images/invitation_utilisateur_4.png
   :height: 147px
   :width: 447px
   :scale: 80%
   :alt: image confirmation invitation
   :align: center
   
L'utilisateur reçoit un mail d'invitation de la GED SAS. Il clique sur le lien "Accepter l'invitation".

* la création manuelle d'un archiviste implique l'ajout manuel de ses droits en tant que contributeur sur les sites versants afin qu'il puisse intervenir sur les versements.

.. image:: ../images/configarchiviste.png
   :height: 147px
   :width: 447px
   :scale: 80%
   :alt: un agent
   :align: center


Préparer le site service versant
=================================

* Se connecter dans la GED SAS avec le compte utilisateur créé pour le gestionnaire du site versant (contact référent associé dans le référentiel)
* Aller dans mes tâches et accepter l'invitation à rejoindre le site

Pour chaque service versant créé dans Alfresco, une synchronisation est réalisée avec les profils utilisables créés dans le référentiel. Pour chaque profil associé à un service donné dans le référentiel, un dossier de type « profilable » est créé à la racine de l’espace documentaire correspondant.

Suite à la synchronisation des données, deux fichiers XML sont créés dans le répertoire « Dictionnaires de données » Alfresco : « synchroVocabulaires.xml » et « synchroProfils.xml » :

.. image:: ../images/fichiersRapportSynchro.png
   :height: 263px
   :width: 601px
   :scale: 80%
   :alt: écran synchronisation
   :align: center

Ces fichiers spécifient deux éléments

* La date de dernière synchronisation pour chaque élément concerné (profils et vocabulaires contrôlés). Cette information permet de lancer les synchronisations ultérieures en mode différentiel permettant de ne récupérer que les éléments ayant été modifiées ou ajoutés depuis la dernière synchronisation.
* Les éléments n’ayant pu être synchronisés durant le processus de synchronisation globale. Ces éléments sont alors tracés dans le fichier XML et seront traités durant la prochaine synchronisation.

Créer un accord de versement par défaut dans la GED SAS
==================================================================

Cette fonctionnalité est uniquement accessible aux gestionnaires de sites. L’accord de versement doit être spécifié pour tous les «sites archive » et « sites archive versant ».

1. Cliquer sur « Modifier les détails du site », en haut à droite de la barre de navigation (icône engrenage)

2. Renseigner le champ accord de versement en recopiant l’identifiant de l’accord de versement créé dans as@lae

3. Ne pas modifier la visibilité (cochée "privée" par défault)

.. image:: ../images/fenetresaisieAccordversement.jpg
   :height: 381px
   :width: 384px
   :scale: 80%
   :alt: paramétrage accord versement ged sas
   :align: center

4. Cliquer sur OK


Synchronisation manuelle spécifique
=================================

Elle permet d’exécuter manuellement le processus de synchronisation avec le module Référentiel pour le site versant concerné ou pour le site d'archive concerné et ses sites versants dépendants.
Ce type de synchronisation est soumise aux mêmes règles d'execution que la synchronisation globale.

Elle met à jour uniquement le fichier « synchroProfils.xml ».

Les éléments de synchronisation sont repertoriés dans un sous élément site (QU'EST CE QUE CA SIGNIFIE ?)

**Synchronisation des profils SEDA**

.. image:: ../images/profilSeda.png
   :height: 113px
   :width: 331px
   :scale: 80%
   :alt: écran synchronisation
   :align: center
   

Les profils SEDA existant à l'état publié dans le module Référentiel sont synchronisés. Ils sont exportés au format XSD et stockés dans le répertoire « Dictionnaire de Données/Profils SEDA » de l’Entrepôt Alfresco :

.. image:: ../images/sedaEntrepotGED.png
   :height: 546px
   :width: 741px
   :scale: 80%
   :alt: écran synchronisation
   :align: center
   
Le fichier « synchroProfils.xml » créé dans le répertoire « Dictionnaire de données » permettant de spécifier la date de dernière synchronisation des profils est alors mis à jour.

.. image:: ../images/synchroProfilXml.png
   :height: 282px
   :width: 1064px
   :scale: 80%
   :alt: écran synchronisation
   :align: center
   
Dans le cas où certains profils n’ont pu être synchronisés, le fichier XML intègre également la liste des de ces profils en échec afin de les prendre en charge durant la synchronisation suivante.
Durant les synchronisations différentielles, si un profil existant est mis à jour dans le Référentiel, une nouvelle version de ce dernier est créée dans la GED SAS, ce qui permet de conserver un historique pour chaque profil.


**Synchronisation des vocabulaires contrôlés**

.. image:: ../images/vocControles.png
   :height: 295px
   :width: 415px
   :scale: 80%
   :alt: un agent
   :align: center

Seuls les vocabulaires contrôlés à l’état publié sont synchronisés avec la GED SAS. Les vocabulaires contrôlés et les concepts définis correspondent dans la GED SAS aux catégories spécifiées dans la catégorie racine « Vocabulaires » :

.. image:: ../images/ecranCategorieVoc.png
   :height: 519px
   :width: 537px
   :scale: 80%
   :alt: catégories et vocabulaires
   :align: center

Durant la phase de synchronisation spécifique, si un concept ou un vocabulaire est modifié, la catégorie correspondante est supprimée de la GED SAS puis recréée selon la nouvelle définition du référentiel.

Le fichier « synchroVocabulaires.xml » créé dans le « Dictionnaire de données » est mis à jour lors de chaque synchronisation. Il spécifie notamment la date de dernière synchronisation, la liste des vocabulaires n’ayant pu être synchronisés pour les traiter durant la synchronisation suivante :

.. image:: ../images/resultatSynchroVoc.png
   :height: 449px
   :width: 1063px
   :scale: 80%
   :alt: résultat synchro vocabulaires
   :align: center


Gérer les processus d'archivage
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Préparer un versement avec un formulaire
=========================================

La préparation d’un versement est la première étape du cycle de vie documentaire. Cette action permet de constituer un pré-versement correspondant à un profil d’archivage. Ce pré-versement est ensuite soumis par le service versant à un workflow de versement vers le module d'archivage et de conservation.

-------------------------------------------------------------------------------------
Initier le pré-versement
-------------------------------------------------------------------------------------

1. Cliquer sur « Préparer un versement ».
Cette icône est disponible à deux emplacements :

* Dans la rubrique « Mes actions » du tableau de bord du site « Service Versant »

.. image:: ../images/icone_versement_1.PNG
   :height: 132px
   :width: 525px
   :scale: 80%
   :alt: icone versement
   :align: center


* Sur l’espace documentaire du site « Service Versant » 

.. image:: ../images/icone_versement_2.PNG
   :height: 132px
   :width: 525px
   :scale: 80%
   :alt: icone versement
   :align: center

2. Une boite de dialogue apparait permettant de sélectionner le profil d’archive souhaité dans une liste déroulante. Cliquer sur OK pour générer un formulaire de versement.

.. image:: ../images/choisirProfil.png
   :height: 132px
   :width: 525px
   :scale: 80%
   :alt: choisir profil versement ged sas
   :align: center

-------------------------------------------------------------------------------------
Compléter le formulaire de versement
-------------------------------------------------------------------------------------

Le formulaire de versement est automatiquement généré en fonction du profil SEDA sélectionné.

Les champs obligatoires (désignés par un astérisque) doivent être complétés

* Pour ajouter un élément facultatif, cliquer sur le bouton « plus » bleu (1)
* Pour supprimer un élément facultatif, cliquer sur la croix rouge (2)
* Pour faire apparaitre les commentaires d’aide à la saisie, cliquer sur le point d’interrogation à côté du champ concerné (3)
* Les chiffres rouges apparaissant dans l'arborescence de gauche indiquent le nombre de champs obligatoires à renseigner (4)
* Les boutons en haut à droite du formulaire permet de passer du mode complet au mode simplifié. Le mode simplifié ne fait apparaitre que les champs qui ne sont pas pré-saisis dans le profil (5)
* Une icône dans le fil d'ariane permet de visualiser le profil dans le module référentiel (6)

Les valeurs renseignées lors de la création du profil SEDA dans le référentiel sont les valeurs définitives et sont donc non modifiables dans la GEDSAS.

.. image:: ../images/formulaireVersementGedSas.PNG
   :height: 480px
   :width: 1031px
   :scale: 80%
   :alt: formulaire versement ged sas
   :align: center


-------------------------------------------------------------------------------------
Joindre les fichiers à verser
-------------------------------------------------------------------------------------

La zone documents permet de joindre les pièces à verser en accompagnant les utilisateurs sur les formats attendus. En effet, la gedsas récupère les types mimes et les formats autorisés spécifiés dans le profil. Lors de la création ou la modification d'un versement, un message renseigne donc l'utilisateur sur le type d'extension attendu.

.. image:: ../images/choixExtension.PNG
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: choix extension
   :align: center


Après le téléchargement du fichier, le module effectue automatiquement deux actions :

1. Contrôle des types de fichier

Si l'extension du fichier choisi ne correspond pas à celle attendue, un message d'avertissement apparaît

.. image:: ../images/mauvaisChoixExtension.PNG
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: mauvais choix extension
   :align: center


Si l'utilisateur appuie sur "OK", le fichier se télécharge et un nouveau message apparaît, sans bloquer le processus de versement en cours.

.. image:: ../images/mauvaisChoixExtension2.PNG
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: mauvais choix extension 2
   :align: center


2. Calcul des dates

Pour chaque unité d’archives, les dates extrêmes sont automatiquement définies par la date de dernière modification des documents téléchargés. 

La date de début est la date de création ou de dernière modification du document le plus ancien.

La date de fin est la date de création ou de dernière modification du document le plus récent.

La date de fin renseigne automatiquement la date de départ de calcul de la DUA et de la communicabilité.


-------------------------------------------------------------------------------------
Enregistrer le versement
-------------------------------------------------------------------------------------

Enregistrer en mode brouillon
##############################

Cette fonctionnalité permet à l'utilisateur d'enregistrer un versement en cours de préparation (icône de dossier jaune). Il pourra reprendre ultérieurement cette préparation en choississant de l'action "Modifier" sur le tableau de bord "Mes traitements".


.. image:: ../images/modificationBrouillon.PNG
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: modification brouillon
   :align: center

On peut aussi modifier un versement préalablement enregistré en mode brouillon en y accédant par l'espace documentaire.

Lorsque l'on revient sur le versement ayant été enregistré en brouillon, les messages d'avertissement relatifs au format des fichiers sont conservés.

Enregistrer un versement finalisé
##################################

Quand l’utilisateur considère le versement comme complet il peut enregistrer le bordereau via le bouton « Enregistrer le bordereau de versement ». L'icône du dossier passe alors en orange.

Le workflow de versement entre le service versant et le service archives peut alors démarrer.

Nom des versements :

* Si le champs commentaire du nom du profil est renseigné avec une valeur définie, il est non modifiable dans le formulaire de la GED SAS et envoyé sans modification vers as@lae. La valeur est associés à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco. Cette valeur est désigne le dossier du versement dans la dashlet « Mes traitements ».
* Si le champs commentaire du nom du profil est obligatoire avec une valeur libre (cardinalité 1), le champs est modifiable et obligatoire dans le formulaire de la GED et la valeur saisie est envoyée vers as@lae. La valeur saisie est aussi associée à un timestamp pour générer un nom de dossier unique dans l’espace documentaire Alfresco.
* Si le champs commentaire du nom du profil est demandé avec une valeur libre non obligatoire (cardinalité 0-1), le champs est modifiable dans le formulaire de la GED SAS. Si la valeur est saisie, le comportement précédant s'applique. Si la valeur n’est pas saisie, le comportement qui suit s'applique. 
* Si le champs commentaire du nom du profil n’est pas demandé, le champs n’existe pas dans le formulaire. Un nom système « Versement » associé à un timestamp est créé. Aucune balise sur le commentaire n'est transmise à as@lae.

Lors de la création ou la modification du versement, Il peut y avoir des erreurs majeures au moment de l'enregistrement. 
Par exemple si le service ARK ne répond pas, pour attribuer un identifiant à chaque éléments du versement (archive, documents..).
L'enregistremment ne sera pas effectué et un message d'alerte préviendra l'utilisateur : 

.. image:: ../images/erreur_technique.png
   :height: 200px
   :width: 650px
   :scale: 80%
   :alt: erreur_technique_versement
   :align: center

-------------------------------------------
Soumettre le versement au service Archives
-------------------------------------------

La soumission d’un versement s’effectue depuis un site versant. Par cette action, un service versant propose un versement au service archives.

Elle peut être lancée au niveau du dossier de versement, de la dashlet « mes traitements » ou de la dashlet « mes actions ».

.. image:: ../images/soumettreVersement.png
   :height: 123px
   :width: 829px
   :scale: 80%
   :alt: soumettre versement ged sas
   :align: center

mettre les 3 images avec les éléments surlignés.

----------------------
Accepter le versement
----------------------

Le(s) coordinateur(s) du site d’archive voi(en)t apparaître une nouvelle tâche dans la dashlet « Mes tâches » du site et au niveau de la dashlet des traitements:

S'il existe un seul gestionnaire du site archives, la tâche lui est assignée automatiquement.

S'il en existe plusieurs, les propositions de versements se placent dans la dashlet "Mes tâches". Chaque gestionnaire doit d'abord s'assigner un versement soumis afin de le traiter.

placer image d'assignation de la tâche pour récupérer un versement

Pour procéder à la validation, cliquer sur cette tâche, puis sur le bouton « Accepter » :


.. image:: ../images/validationVersement.png
   :width: 849px
   :height: 488px
   :scale: 80%
   :alt: validation versement ged sas 
   :align: center


Une fois le versement soumis, l’icône du dossier prend la couleur bleue.

.. image:: ../images/versementSoumis.png
   :height: 131px
   :width: 826px
   :scale: 80%
   :alt: versement ged sas soumis 
   :align: center


----------------------
Valider le versement
----------------------


Suite à l'acception du versement par le service archives, le gestionnaire du site versant voit s’afficher une tâche « Confirmation du versement ».

Dans la dashlet "Mes traitements" du service versant, cliquer sur le bouton « Valider le versement ».

.. image:: ../images/validerVersement.png
   :width: 855px
   :height: 364px
   :scale: 80%
   :alt: confirmation validation versement ged sas 
   :align: center

Suite à cette opération, la taille des documents versés est vérifiée. 

Si la taille des documents est inférieure à 300 Mo, le versement est transmis directement au module de gestion de l'archivage et de la conservation.

Si la taille des documents est supérieure à 300 Mo, le versement passe dans un statut « En attente fichier volumineux » (icône marron). Un processus spécifique de gestion des fichiers volumineux se lance. A son achèvement, l'icône repasse en bleu.

La validation du versement équivaut à la signature du bordereau par le service versant. Elle envoie le versement dans le module de gestion de l'archivage et de la conservation, où il sera traité par le service archives :voir: `Accepter un transfert dans Asalae <http://saem.readthedocs.io/fr/master/guide-utilisateur/asalae.html#accepter-un-transfert-dans-asalae>`_



----------------------
Corriger un versement
----------------------


Lorsqu’un versement est rejeté par le service archive à partir de la GED SAS ou d’as@lae, une tâche de correction du versement est assignée à l’auteur du versement.

.. image:: ../images/refusVersement.png
   :width: 495px
   :height: 178px
   :scale: 80%
   :alt: refus validation versement ged sas 
   :align: center

L’auteur du versement peut alors le modifier et le soumettre à nouveau au service archives.


Préparer un versement manuel
=============================

La GED SAS permet de créer des versements de façon manuelle (sans passer par le formulaire). Cette possiblité sera principalement utilisée par les archivistes.REPRENDRE ICI

L’utilisateur peut ainsi préparer un versement à partir d’un ou plusieurs « vracs numériques » tout en bénéficiant des validations de profils déjà mis en œuvre au sein de la GED SAS.

Extension du modèle de données Alfresco
=============================================

Le modèle de données Alfresco a été personnalisé afin d’adapter le modèle de métadonnées GED aux exigences fonctionnelles du projet. 

6.1.1 Documents
===============

Lors de l’import d’un fichier (via bouton « Importer dans l’entrepôt » ou drag & drop) dans un dossier « profilable », un formulaire de saisi est imposé à l’utilisateur pour renseigner ces métadonnées :

.. image:: ../images/aspectArchivesDocGedSas.png
   :height: 639px
   :width: 657px
   :scale: 80%
   :alt: aspect archives document ged sas 
   :align: center

Actuellement les propriétés spécifiées sur les documents sont les suivants :

* Nom
* Titre
* Description
* Typologie de document
* Date de validation
* Format
* Contrôle
* Identification Unique (Ark)

La valorisation de l’identifiant unique (ARK) positionné sur le document est automatique et fournie par le Référentiel via la consommation d’un webservice REST soumis à authentification.

.. image:: ../images/identifiantArkDoc.png
   :width: 419px
   :height: 316px
   :scale: 80%
   :alt: identifiant ark document ged sas 
   :align: center

6.1.2 Répertoires
==================
Si l’utilisateur souhaite créer manuellement un répertoire dans un versement (situé implicitement dans un dossier « profilable ») le formulaire de création suivant est affiché : 

.. image:: ../images/creationRepertoireDossierGedSas.png
   :width: 538px
   :height: 852px
   :scale: 80%
   :alt: identifiant ark document ged sas 
   :align: center

Les propriétés suivantes sont alors saisissables :

* Nom

* Titre

* Date de début

* Date de fin

* Description

* Niveau de description 

* Indexation (positionnement des concepts)

* Règle à appliquer (pour la DUA/Sort final)

* DUA

* Date de départ de calcul

* Règle à appliquer (pour l’accès/communicabilité)

* Date de départ de calcul

Les métadonnées positionnées sont alors visibles depuis la vue détaillée du répertoire :

.. image:: ../images/vueDetailleFolderGEDSas.png
   :width: 538px
   :height: 621px
   :scale: 80%
   :alt: propriétés dossiers document ged sas 
   :align: center

Identiquement aux documents, un identifiant ARK est automatiquement récupéré depuis le Référentiel et positionné sur le répertoire.

6.2   Création manuelle d’un versement
======================================

La création manuelle d’un versement peut se faire dans un site versant et dans un versement en cours (répertoire créé sous le répertoire « profilable »)  via les actions présentées précédemment à savoir : 

* « Importer dans l’entrepôt » permettant l’import d’un document.
* « Drag&Drop » : Glisser-Déposer d’un document permettant son import dans la GED SAS
* « Créer un dossier » : permettant la création d’un répertoire dans le versement en cours. 

L’utilisation de ces actions permet d’enrichir les propriétés des éléments importés/créés via les métadonnées spécifiques et les formulaires présentés précédemment.

6.3   Import de versement via archive ZIP
=========================================

La création manuelle d’un versement peut être également réalisée via l’import d’une archive ZIP dans un dossier « profilable ». L’utilisateur prépare alors sur son poste l’arborescence documentaire associée au versement suivant le profil cible :

.. image:: ../images/cibleZipDossier.png
   :width: 198px
   :height: 106px
   :scale: 80%
   :alt: propriétés dossiers document ged sas 
   :align: center

Pour chaque unité documentaire, les fichiers d’archives sont ajoutés. 
L’utilisateur crée alors le zip à partir de cette arborescence :

.. image:: ../images/creationArboZip.png
   :width: 609px
   :height: 100px
   :scale: 80%
   :alt: création dossier zip pour ged sas 
   :align: center

Le fichier zip est alors importé dans le dossier profilable du site versant associé :

.. image:: ../images/importerDossierZipGedSas.png
   :width: 515px
   :height: 267px
   :scale: 80%
   :alt: import dossier zip pour ged sas 
   :align: center

Pour rappel, les dossiers « profilable » sont situés à la racine de l’espace documentaire de chaque site versant. 
L’utilisateur procède alors à la décompression de l’archive via l’action « Décompresser » :

.. image:: ../images/decompressionZipGedSas.png
   :width: 747px
   :height: 303px
   :scale: 80%
   :alt: decompression dossier zip pour ged sas 
   :align: center

Le système affiche la boîte de dialogue « Extraire vers… » :

.. image:: ../images/extraireGedSas.png
   :width: 813px
   :height: 259px
   :scale: 80%
   :alt: extraction dossier zip pour ged sas 
   :align: center

Suite à la décompression :
* Un répertoire « Versement 1 » est créé. Ce dernier contient l’ensemble de l’archive ZIP importé.
* L’archive ZIP est supprimée de l’espace documentaire du site.
* Un identifiant ARK est attribué pour chaque élément (document/répertoire) du versement.

.. image:: ../images/creationDossierExtraction.png
   :width: 559px
   :height: 180px
   :scale: 80%
   :alt: creation dossier zip pour ged sas 
   :align: center

Durant la décompression de l’archive ZIP, un contrôle de conformité est réalisée pour vérifier la conformité du versement décompressé avec le profil SEDA défini par le dossier.
Dans cet exemple, le profil SEDA attend l’arborescence documentaire suivante :

* Séance de l’assemblée
   * accuse
   * délibération
   * signature

Les règles actuelles mises en œuvre durant le contrôle de conformité sont les suivantes :

* Si un répertoire autre que ceux définis dans le profil SEDA est présent dans l’archive ZIP, il est décompressé puis supprimé (Par exemple : existence d’un répertoire « temp » sous « Séance de l’assemblée »).
* Si un répertoire de l’archive ZIP contient n documents et que le profil stipule une cardinalité de 1 pour ce répertoire, l’ensemble des documents sont décompressés et conservés car aucune information complémentaire ne permet à ce jour de définir le seul fichier à retenir dans le lot.

L’ensemble de ces règles pourront évoluer selon les besoins fonctionnels.
L’apport d’un fichier XML complémentaire au ZIP pourra être envisagé pour définir les métadonnées associées à chacun des éléments du versement.

6.4 modification d'un versement déjà déposé via formulaire
==========================================================

Ce point a été abordé au paragraphe 5.9 :ref:`modif_versement`

6.5   Indicateur de complétude
==============================

Pour chaque versement créé manuellement au sein de la GED SAS, une vérification est effectuée pour contrôler l’éventuelle complétude du versement en fonction de la définition du profil SEDA qui lui est associé.
Si le versement est considéré comme « à priori » complet (ie. l’ensemble des métadonnées et fichiers obligatoires sont spécifiés dans le versement) , un indicateur est ajouté sur le dossier du versement :


.. image:: ../images/indicateurCompletude.png
   :width: 457px
   :height: 185px
   :scale: 80%
   :alt: indicateur complétude versement ged sas 
   :align: center

Cette information permet ainsi d’informer l’utilisateur que le versement effectué manuellement est conforme « à minima » au profil SEDA associé.

7  Suivi des traitements
========================
7.1   Statut des dossiers

Un code de couleur est associé à chacun des statuts des dossiers d’un versement ou d’une archive.

* gris : dossier en attente de prise en compte par as@lae
* jaune : Versement en cours de préparation – mode brouillon
* orange : Versement complet– à  verser
* bleu : Versement en cours de traitement dans un workflow 
* vert : Archive ou dossier de versement accepté dans as@lae
* violet : Archive en cours de restitution
* prune : Archive restituée
* rouge : Archive en cours d'élimination
* jaune avec croix rouge : archive éliminée
* turquoise : dossier en cours de communication
* bleu azur : dossier communiqué
* marron : fichier volumineux en attente d'upload


7.2   Dashlet « Mes traitement »
================================

La dashlet « Mes traitements » est disponible dans le tableau de bord des sites versants ou archive.

.. image:: ../images/dashletMesTraitements.png
   :width: 825px
   :height: 233px
   :scale: 80%
   :alt: dashlet es traitements ged sas 
   :align: center

Cette dashlet présente la liste des dernière archives créées / modifiées par tous les membres des groupes de l’utilisateur courant et les actions à effectuer par l’utilisateur. Les dossiers présentés sont les dossiers impliqués dans un workflow ou les dossiers modifiés depuis moins de sept jours.
Dans un site archive, la colonne « Auteur » est remplacée par « Service versant ».
Les éléments affichés peuvent être trié suivant la donnée associée à une colonne en cliquant sur l’entête de celle-ci.
Les actions disponibles  sont présentées sur la colonne de droite.

.. csv-table:: action dashlet traitements
   :header: "action", "description", "icône"
   :widths: 200, 400

   "Modifier", "Modification d’un bordereau de versement ou d’archive", "crayon"
   "verser", "Lancement du workflow de versement", "+"
   "Valider", "Validation d’un versement un membre du service archive", "cocher"
   "confirmer", "Confirmation d’un versement", "flèche"
   "verser", "Versement corrigé à verser", "+ (rouge)"
   "Eliminer", "Action positionnée pour une archive éliminable dans la DUA est expirée. Elle permet de transmet directement une demande d’élimination à as@lae. Dans un site archive elle ne sera présentée que lorsque le service archive pourra procéder à l’élimination de l’archive ?", "X"
   "Restituer", "Action positionnée pour une archive versée Transmission de la demande de restitution à as@lae", "recycler"
   "confirmer restitution", "Action notifiée au service archive. Elle permet d’informer d’une demande de restitution.", "flèche violette"
   "Confirmer Elimination ", "Demande d’élimination proposée au niveau du site archive pour une archive dont la DUA est expirée provenant d’un autre site versant.", "X (verte)"
   "valider élimination", "Validation de l’élimination par le service versant", "X (orange)"
   "Changer les DUA", "Action positionnée lorsque le service archive doit effectuer un changement de DUA", "sablier"

7.3   Filtre de l’espace documentaire
======================================

   Il est possible de rechercher les différents types de dossiers dans l’espace documentaire via le menu de gauche. Un filtre est disponible pour chacun des statuts de dossiers cités plus haut.

.. image:: ../images/filtreEspaceDoc.png
   :width: 927px
   :height: 282px
   :scale: 80%
   :alt: filtre espace doc ged sas 
   :align: center


9  Workflow de restitution
==========================

9.1   Action de restitution dans la GED
=======================================

Une archive versée peut être entièrement restituée. L’action de restitution peut être lancée de trois endroits différents :

1. Depuis la dashlet des traitements si elle a été versée depuis moins de sept jours
2. Depuis la dashlet « mes actions »

.. image:: ../images/restitutionVersement.png
   :width: 521px
   :height: 315px
   :scale: 80%
   :alt: restitution versement ged sas 
   :align: center

3. Depuis l’espace documentaire au niveau des actions proposées sur les archives versées. 
Le filtre sur les documents permet de visualiser les archives versées.

.. image:: ../images/flitreArchivesVersees.png
   :width: 986px
   :height: 432px
   :scale: 80%
   :alt: filtre archives versées ged sas 
   :align: center

Une fois la restitution demandée, la demande est transmise directement à as@lae et une nouvelle tâche est affectée aux membres du service archive.

9.2   Prise en compte de la demande de restitution par le service archive
===========================================================================

Lorsqu’une demande de restitution est effectuée, les membres du service archive voient une nouvelle tâche dans leur liste de traitements.

.. image:: ../images/workflowRestitution.png
   :width: 493px
   :height: 176px
   :scale: 80%
   :alt: workflow restitution archives 
   :align: center

Cette tâche est une simple notification pour signaler une demande de restitution sur un dossier.

10 Workflow de communication
============================

10.1  Action de communication dans la GED
=========================================

Une archive versée non éliminée ou restituée peut être totalement ou partiellement restituée :

1. Depuis la dashlet des traitements si elle a été versée depuis moins de sept jours en cliquant sur l’icône .
 
.. image:: ../images/workflowComDashlet.png
   :width: 922px
   :height: 26px
   :scale: 80%
   :alt: workflow demande de communication archives 
   :align: center


2. Depuis l’espace documentaire au niveau des actions proposées sur les archives versées ou des unités documentaires. 
Le filtre sur les documents permet de visualiser les archives versées.

.. image:: ../images/filtreArchivesCoommunicables.png
   :width: 986px
   :height: 432px
   :scale: 80%
   :alt: filtre communication archives 
   :align: center

Une fois la communication demandée, la demande est transmise directement à as@lae et une nouvelle tâche est affectée aux membres du service archive.

10.2   Prise en compte de la demande de communication par le service archive
============================================================================

Lorsqu’une demande de communication est effectuée, les membres du service archive voient une nouvelle tâche dans leur liste de traitements.

.. image:: ../images/workflowComm.png
   :width: 496px
   :height: 142px
   :scale: 80%
   :alt: workflow communication archives 
   :align: center 
 
Cette tâche est une simple notification pour signaler une demande de communication sur un dossier.





11 Workflow d’élimination
==========================

Une demande d’élimination peut être effectuée pour les archives versées dont la DUA est expirée.

.. _warning:
Pour qu’une demande d’élimination puisse être traitée dans as@lae, l’outil de conversion de document Cloudoo doit être correctement paramétré.

11.1   Elimination depuis un site versant
=========================================

L’action d’élimination peut être lancée de trois manières différentes :

1. Depuis la dashlet des traitements, un icône X est disposé à côté des actions possibles.
2. Depuis la dashlet « mes Actions », les dossiers éliminables sont affichés

.. image:: ../images/demandeElimination.png
   :width: 529px
   :height: 209px
   :scale: 80%
   :alt: workflow élimination archives 
   :align: center

3. Depuis l’espace documentaire, les filtres proposés permettent de visualiser les dossiers versés mais ne prennent pas compte de l’expiration des DUA

11.2   Elimination depuis un site archive
==========================================

Les archives dont la DUA est expirées sont présentées au niveau de la dashlet mes traitements.
Lorsque l’archive expirée a été versée depuis le site archive (cas d’un site archive versant) l’action d’élimination peut être lancée depuis la dashlet « mes traitements », la dashlet « mes actions » ou l’espace documentaire.

**Lancement du workflow**

Lorsque l’archive expirée a été versée depuis un site versant rattaché au site archive, le service archive ne peut lancer l’action d’élimination sans l’approbation du service versant. Une icône X est disposée au niveau des actions possibles de la dashlet « mes traitements ». Elle permet de lancer le workflow de restitution.

.. image:: ../images/detailWFElimination.png
   :width: 703px
   :height: 303px
   :alt: détails workflow élimination archives 
   :align: center

Le formulaire de lancement du workflow se présente ainsi. 

.. image:: ../images/interfaceWFElimination.png
   :width: 852px
   :height: 322px
   :scale: 80%
   :alt: interface workflow élimination archives 
   :align: center

**Validation du service versant**

Une fois la demande d’élimination effectuée, une nouvelle tâche est affectée aux utilisateurs du site versant.
Elle est aussi présentée au niveau de la dashlet « mes traitements » avec l’icône X.
Le service versant peut alors confirmer l’élimination ou demander un changement des DUA de l’archive.

**Changement des DUA**
Lorsque le service versant demande un changement des DUA, une nouvelle tâche de changement des DUA est affectée au service archive.
L’icône sablier est présentée dans la dashlet « mes traitements » au niveau des actions possibles sur le dossier. Le changement de DUA devra être effectué dans as@lae. Une fois la modification effectuée, le service archive pourra terminer la tâche en cliquant sur l’icône.

**Elimination**
Lorsque le service versant confirme l’élimination, une nouvelle tâche d’élimination est affectée au service archive.
L’icône X est présentée dans la dashlet « mes traitements » au niveau des actions possibles sur le dossier. Elle permet de terminer la tâche et d’envoyer la demande d’élimination à as@lae.

12 Profil du Site
===================

Une dashlet « Profil du site » présente les informations d’archivage relatives au site :

* L’intitulé du site,
* Le nom du service archive pour un service versant,
* La liste des profils disponibles sur ce site,
* La visibilité du site.

L’icône renvoie vers la page de consultation de l’élément à côté duquel il est apposé dans le référentiel.
Voici un exemple de profil de site.

.. image:: ../images/exempleProfilSiteVersant.png
   :width: 495px
   :height: 401px
   :scale: 80%
   :alt: exemple profil site versant archives 
   :align: center

13 Recherche Avancée
======================

Le formulaire de recherche avancée est accessible uniquement aux utilisateurs appartenant au groupe des administrateurs. Il est accessible à partir de la barre de menus (en haut à droite).

.. image:: ../images/lienFormSearch.png
   :width: 749px
   :height: 96px
   :scale: 80%
   :alt: lien formulaire recherche 
   :align: center

La recherche avancée propose un formulaire spécifique en fonction du type de données recherchées.

.. image:: ../images/typeContenuSearch.png
   :width: 880px
   :height: 306px
   :scale: 80%
   :alt: type contenu formulaire recherche 
   :align: center

Il faut choisir le type de contenu « Dossiers d’archives » pour effectuer des recherches sur les archives. Le formulaire présente un champ de saisie ou de sélection pour les différentes informations pouvant caractérisée une archive.

**Enregistrement des recherches**

Un bouton "sauvegarder" se trouvant en haut et en bas du formulaire permet d’enregistrer les critères d’une recherche. La liste des recherches avancées peut être gérée depuis la dashlet « Mes recherches ». Un utilisateur devra l’ajouter pour la visualiser.

La croix permet de supprimer une recherche.
Pour lancer une recherche, il faut cliquer sur non nom.

**Présentation des résultats de recherche**

Les résultats de recherches sont proposés sous forme de liste avec une navigation à facettes permettant de trier facilement les résultats. 

.. image:: ../images/facetteRecherche.png
   :width: 336px
   :height: 347px
   :scale: 100%
   :alt: facettes formulaire recherche 
   :align: center

14    Intégration de flux PESv2
===============================

14.1  Modèle de Profil PESv2
=============================
Un modèle de profil spécifique est prévu pour le versement de fichier PES.

14.1.1   Convention de nommage
==============================
Les profils créés dans le référentiel pour le versement de fichier PESv2 devront comporter le mot clé « PES ». Par exemple, ils pourront s’appeler « PESv2 », « flux PESv2 Bordeaux », «  PES Gironde » … etc.

14.1.2   Exemple de profil PES
==============================
Le profil est constitué d’une unité d’archive (cardinalité 1) comportant deux unités documentaires de cardinalité 0..n. Chacune de ces unités documentaires contiendra exactement deux flux PES.

.. image:: ../images/profilPES.png
   :width: 259px
   :height: 209px
   :scale: 100%
   :alt: profil PES
   :align: center

Les éléments entre accolades correspondent à des variables qui seront positionnées par le processus de versement automatisé depuis  la GED SAS. L’unité commençant par PES_BJ contiendra deux flux PES_BJ et PES_Acquit, l’unité documentaire commençant par PES_PJ contiendra deux flux PES_PJ et PES_Acquit.
Les captures d’écran ci-dessous permettent de visualiser les différents éléments du profil.

.. image:: ../images/accoladeProfilPes.png
   :width: 801px
   :height: 230px
   :scale: 100%
   :alt: paramétrage profil PES
   :align: center

   .. image:: ../images/dateFluxPes.png
   :width: 806px
   :height: 337px
   :scale: 100%
   :alt: date profil PES
   :align: center

.. image:: ../images/uniteDocFluxPes.png
   :width: 815px
   :height: 375px
   :scale: 100%
   :alt: unité doc profil PES
   :align: center

.. image:: ../images/fichierFluxPes.png
   :width: 804px
   :height: 413px
   :scale: 100%
   :alt: unité fichier profil PES
   :align: center

Les deux objets de données des unités documentaires ont tous une cardinalité 1.

.. image:: ../images/cardinalitePes.png
   :width: 435px
   :height: 223px
   :scale: 100%
   :alt: cardinalité CDO profil PES
   :align: center

14.1.3   Paramètres du profil
=============================

Les éléments des profils entre accolades correspondent à des paramètres qui seront renseignés lors de la constitution depuis la  GED SAS.

+--------------------+-----------------------------------+------------------------+------------------+
|Nom du paramètres   | description                       |Exemple Flux PES        |  Valeur          |
+====================+===================================+========================+==================+
|Annee               |Année du versement                 |                        |2016              |
+--------------------+-----------------------------------+------------------------+------------------+
|CodBud              |Code Budget                        |<CodBud V="00"/>        | 00               |
+--------------------+-----------------------------------+------------------------+------------------+
|CodColl             |Code Collectivité                  |<CodCol V="100"/>       | 100              |
+--------------------+-----------------------------------+------------------------+------------------+
|Collectivite        |Nom de la collectivité             |                        |Commune de        |  
|                    |                                   |                        |Saint Etienne     |
+--------------------+-----------------------------------+------------------------+------------------+
|CollectiviteFormatee|Nom de la collectivité             |                        |Saint-Etienn      |
|                    |sans espaces et caractères         |                        |                  |
|                    |accentués.                         |                        |                  |
|                    |Les espaces sont remplacés         |                        |                  |
|                    |par des « _ »                      |                        |                  |
+--------------------+-----------------------------------+------------------------+------------------+
|Domaine             |Domaine budgétaire. Il est         |<PES_RecetteAller>      |recette           |
|                    |déterminé en fonction du nom de la |                        |                  |
|                    | balise PES_RecetteAller ou        |                        |                  |
|                    | PES_DepenseAller                  |                        |                  |
+--------------------+-----------------------------------+------------------------+------------------+
|DteStr              |Date de génération du flux         |<DteStr V="2015-02-03"/>|   2015-02-03     |
+--------------------+-----------------------------------+------------------------+------------------+
|IdBord              |Identifiant du bordereau           |<IdBord V="875"/>       |875               |
|                    |  (BJ uniquement)                  |                        |                  |
+--------------------+-----------------------------------+------------------------+------------------+
|IdPost              |Poste de dépense                   |<IdPost V="033017"/>    |033017            |
+--------------------+-----------------------------------+------------------------+------------------+
|Jour                |Jour de dépôt du versement         |                        |17 (pour le 17ème |
|                    |dans le mois.                      |                        |jour du mois)     |
+--------------------+-----------------------------------+------------------------+------------------+               
|Mois                |Mois de dépôt du versement         |                        |08 (pour le mois  |
|                    |                                   |                        |d’Août)           |
+--------------------+-----------------------------------+------------------------+------------------+ 
|NomFic              |Nom du fichier PES Aller           |<NomFic                 |PES120150203133381|
|                    |                                   |V="PES120150203133381"/>|                  |
+--------------------+-----------------------------------+------------------------+------------------+
|TypBord             |Type de bordereau                  |<TypBord V="01"/>       | 01               |
+--------------------+-----------------------------------+------------------------+------------------+

14.2  Dépôt des Flux 
====================
14.2.1   Plan de classement
===========================

Les fichiers PESv2 seront déposés dans un dossier nommé « PESv2 » situé dans l’entrepôt.  L’arborescence des dossiers PES est mise à jour lors de chaque synchronisation du référentiel.
Un sous dossier sera créé pour chaque autorité administrative (collectivité) du référentiel qui aura au moins une unité administrative (service d’archive ou versant) publiée. Pour que les versements puissent être réalisés, l’autorité d’archivage devra au moins avoir un service archive et un service versant producteur publié.
Si un profil respectant les conventions de nommage PES est rattaché à un service versant (publié) de la collectivité alors un dossier portant le nom du profil sera créé dans le dossier PESv2 de la collectivité.
Les fichiers PES seront déposés dans une arborescence du type PESv2/{collectivité}/{profil PES}/AAAA/MM/JJ .

.. image:: ../images/arboPES.png
   :width: 258px
   :height: 285px
   :scale: 100%
   :alt: arbo PES
   :align: center

14.2.2   Réception des fichiers
===============================

Un web service a été mis en place pour la réception des fichiers. Il prend en compte deux paramètres, le nom de la collectivité et le fichier XML  PESv2. 
Avant d’effectuer un dépôt de fichier, il faudra s’assurer qu’un profil PESv2 a été lié à un service versant de la collectivité ciblé. Refaire une synchronisation manuelle pour mettre à jour l’arborescence des dossiers PES si besoin.
Une fois le fichier déposé, le web service retourne une réponse au format XML avec un code 200 si le fichier a bien été enregistré.
 
14.2.3   Versement des flux PESv2
==================================

Une tâche planifiée est prévue quotidiennement pour assurer le versement des fichiers PES.
Les fichiers déposés sont regroupés en fonction de leur « nomFic ». Seuls les paquets de fichier PES_PJ +PES_Acquit  et PES_PJ+PES_Aquit seront pris en compte.
Un dossier de versement  sous le modèle PESv2_{Collectivite}_AAAAMMJJ par exemple 
PESv2_Commune_de_Saint-Etienne_20160818. 
Les versements de flux PES sont réalisés de manière automatique vers as@lae. Aucune action n’est requise de la part du service versant. Une fois acceptés les flux PES apparaîtront come ci-dessous dans le tableau des traitements.

.. image:: ../images/tableauTransfertPES.png
   :width: 972px
   :height: 30px
   :scale: 100%
   :alt: tableau transfert PES
   :align: center

 
Pour faciliter les tests une action permettant de déclencher manuellement la tâche planifiée a été mise en place. Si un dossier de versement a été créé pour un jour donné, il faudra le renommer ou le supprimer dans le site versant pour refaire d’autres  tests le même jour.


