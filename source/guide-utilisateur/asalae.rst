.. _asalae:

.. _asalae_accueil:


Module - gestion de l'archivage et de la conservation
-----------------------------------------------------

Fonctionnalités
~~~~~~~~~~~~~~~~
Ce module permet de valider le versement des paquets d'informations soumis par le module de gestion des processus d'archivage et de les transformer en paquets d'information à archiver.

Une fois connecté, l'utilisateur habilité peut gérer le versement, l'élimination et la conservation des archives électroniques sous sa responsabilité.

.. _asalae_transfert:

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~

Créer les services de synchronisation du référentiel
================================================================

Se connecter en administrateur.
Aller dans administration technique / référentiel extérieur / ajouter, pour créer les services de synchronisation des 4 éléments (collectivités, services, profils, vocabulaires).

.. image:: ../images/synchroAsalae.png
   :width: 868px
   :height: 202px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center
   
Configuration des 4 connecteurs :

**Collectivités**

Asalae gère de multiples collectivités depuis la version 1.6.1. Une collectivité dans Asalae correspond à une autorité administrative dans le référentiel.

Paramètre à renseigner: 

- url d'appel du référentiel

- identifiant de la collectivités de référence

.. image:: ../images/connecteurcollectivites.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

**Services**

Paramètre à renseigner: 

- url d'appel du référentiel

- mapping des roles

.. image:: ../images/connecteurservice.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

**Profils**

Paramètre à renseigner: 

- url d'appel du référentiel

- version du SEDA

.. image:: ../images/connecteurprofil.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

**Vocabulaires**

Paramètre à renseigner: 

- url d'appel du référentiel

.. image:: ../images/connecteurvocabulaire.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center
 

Créer les services d'assignation des identifiants ARK
=================================================================

Pour assigner les identifiants ARK générés par le référentiel aux archives, unités d'archives et objets données, aller dans administration technique / compteur.
Editer les lignes 2 à 5, sélectionner compteur externe comme type de compteur.

.. image:: ../images/asalae_parametrage_compteur_referentiel.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center   
   
.. image:: ../images/asalae_parametrage_compteur_referentiel_2.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center     


Synchroniser des éléments du référentiel
==========================================

La synchronisation des différents éléments peut être lancée individuellement en cliquant sur l’icône loupe présentée dans l’interface puis en exécutant une des deux actions suivantes proposées en bas de page de visualisation du référentiel :

.. image:: ../images/detailSynchroAsalae.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

.. index:: ark

La « mise à jour du référentiel » effectue alors une mise à jour différentielle en synchronisant uniquement les éléments modifiés depuis la dernière synchronisation. Cette fonctionnalité récupère tous les éléments du référentiel quelque soit leur date de modification.
Une tâche planifiée est également disponible depuis le menu  Administration Technique >Tâches planifiées afin d’effectuer une sychronisation complète de l’ensemble des référentiels extérieurs actifs.

Un compte utilisateur sera créé par Asalae pour chaque contact référent d’une unité administrative ayant le rôle archivistique « archive » ou « producteur ». La convention de nommage des identifiants utilise la première lettre du prénom suivie du nom. Par exemple le contact référent « Pierre Daniel » aura l’identifiant de connexion « pdaniel ». Le mot de passe initial est identique à l’identifiant de connexion. Lors de sa première connexion Pierre Daniel pourra utiliser le mot de passe « pdaniel ». 


.. image:: ../images/creationUserAsalae.png
   :width: 732px
   :height: 385px
   :scale: 80%
   :alt: creation user asalae
   :align: center

.. warning:: 
Une réinitialisation du mot de passe sera demandée à la première connexion.



Créer un circuit de traitement pour les transferts d'archives
===============================================================

Les circuits de traitement par défaut sont tous constitués d’une étape de contrôle et d’une étape de validation faisant uniquement intervenir l’utilisateur admin.

1. Aller dans Administration / Circuits de traitement / Ajouter un circuit de traitement.
2. En "Type de circuit", sélectionner "traitement de transfert d'archives"
3. Renseigner le formulaire comme ci-dessous

.. image:: ../images/traitementArchives.png
   :width: 588px
   :height: 297px
   :scale: 80%
   :alt: paramétrage traitement archives
   :align: center

4. Valider
5. Retourner dans la liste des circuits de traitement et aller dans les étapes (drapeau vert) du circuit nouvellement créé.
6. Ajouter une étape "validation" et renseigner le formulaire comme ci-dessous

.. image:: ../images/circuitTraitement.png
   :width: 781px
   :height: 271px
   :scale: 80%
   :alt: circuit traitement archives
   :align: center

7. Valider
8. Cliquer sur l’icône groupe bleu pour composer l’étape 
9. Cliquer sur le lien « Ajouter une composition»
10. Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionArchives.png
   :width: 184px
   :height: 90px
   :scale: 80%
   :alt: type composition archives
   :align: center

11. Valider

Créer un accord de versement
================================

Un accord de versement est obligatoire pour l’acceptation d’un versement. S’il n’est pas spécifié au moment du versement, c’est l’accord de versement par défaut qui sera pris en compte. 
Il est donc nécessaire d’en créer un car aucun accord de versement n’est spécifié lors des transferts effectués depuis la GED SAS.

1. Aller dans le menu Administration SEDA / Accords de versement
2. Cliquer sur “Ajouter un Accord de versement”
3. Remplir les champs obligatoires
   * l'identifiant correspond au champ de la balise "Archival agreement" du profil SEDA (n'utiliser ni espace ni accent). Cet identifiant est à renseigner dans la GED SAS.
   * sélectionner un service archive
   * sélectionner 0, 1 ou plusieurs service(s) versant(s) (CTRL+A)
   * Choisir le volume de stockage principal
4. Cocher « Autoriser tous les producteurs pour cet accord de versement (déclaration d'un contrat pour chaque producteur non nécessaire) » 
5. Valider
6. Editer l’accord de versement créé
7. Dans l’onglet Contrôles, sous « Pièces jointes » cocher « Validation des pièces jointes : en cas de non validité, générer dune aletre au lieu d'une erreur, et considérer le transfert conforme ».


Modifier le circuit « traitement des demandes de communication » (à revoir éventuellement quand cette fonctionnalité sera reprise)
====================================================================================================================================

1. Aller dans Administration / Circuits de traitement
2. Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes de communication »
3. Cliquer sur l’icône  
4. Editer le type 
5. Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center

6. Valider

Modifier le circuit « traitement des demandes d’éliminations » (à revoir éventuellement quand cette fonctionnalité sera reprise)
==================================================================================================================================

1. Aller dans Administration >Circuits de traitement
2. Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes d’élimination»
3. Cliquer sur l’icône   au niveau de l’étape « Accord du service producteur »
4.   Supprimer la composition de type « Service producteur » en cliquant sur   
5.   Cliquer sur le lien « Retour à la liste des étapes »
6.   Supprimer l’étape « Accord du service producteur »
7.   Modifier l’étape « Validation » en cliquant sur l’icône  
8.   Editer le type de composition en cliquant sur  
9.   Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center 

10.   Valider

Modifier le circuit « traitement des demandes de restitution » (à revoir éventuellement quand cette fonctionnalité sera reprise)
===================================================================================================================================

1.   Aller dans Administration >Circuits de traitement
2.   Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes de retitutiob»
3.   Cliquer sur l’icône   au niveau de l’étape « Contrôle »
4.   Supprimer la composition de type « Utilisateur as@lae» en cliquant sur   
5.   Cliquer sur le lien « Retour à la liste des étapes »
6.   Supprimer l’étape « Contrôle »
7.   Modifier l’étape « Validation » en cliquant sur l’icône  
8.   Editer le type de composition en cliquant sur  
9.   Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center

10.   Valider


Traitements à opérer
~~~~~~~~~~~~~~~~~~~~

Accepter un transfert dans Asalae
=================================

Les dossiers transférés apparaissent dans « Mes Transferts à traiter » ou « Transferts non conformes » quelques secondes après avoir été transférés depuis la GED SAS.

1. Se placer dans le sous menu Transferts  Mes transferts à traiter (ou Transferts non conformes)
2. Cocher la case à gauche de la ligne du transfert concernée
3. Choisir « Accepter » dans la liste déroulante
4. Cliquer sur le bouton « Accepter » 


.. image:: ../images/validerversementAsalae.png
   :width: 987px
   :height: 377px
   :scale: 80%
   :alt: confirmation validation versement as@lae 
   :align: center

5. Répéter l’opération autant de fois que nécessaire

.. note::
Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là le dossier de versement aura une couleur verte et le statut versé.


Traiter une demande de restitution
==================================

La demande de restitution apparaît dans la validation des demandes de restitution.
 
Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert. Ce point a été abordé au paragraphe précédent.

Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là l’archive aura une couleur violette et le statut « restituée ».

Traiter une demande d’élimination
=================================

Une demande d’élimination peut être effectuée pour les archives versées dont la DUA est expirée.
Pour qu’une demande d’élimination puisse être traitée dans Asalae, l’outil de conversion de document Cloudoo doit être correctement paramétré.

Elimination depuis un site versant (à basculer côté ged sas)
L’action d’élimination peut être lancée de trois manières différentes :

1. Depuis la dashlet des traitements, une icône est disposée à côté des actions possibles.
2. Depuis la dashlet « mes Actions », les dossiers éliminables sont affichés
3. Depuis l’espace documentaire, les filtres proposés permettent de visualiser les dossiers versés mais ne prennent pas compte de l’expiration des DUA

Traiter une demande de communication
====================================

La demande de restitution apparaît dans la validation des demandes de communication.

.. image:: ../images/traitementComAsalae.png.png
   :width: 768px
   :height: 124px
   :scale: 80%
   :alt: paramétre validité pièce jointe asalae
   :align: center


Les actions à effectuer pour accepter la communication sont similaires à celles du transfert. 

Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là les éléments communiqués seront déposés dans le dossier Communication du service versant. Les dossiers communiqués seront présentés sur le tableau de suivi des traitements avec l’icône . 
 
L’icône   est cliquable et renvoie au dossier contenant les éléments communiqués.

