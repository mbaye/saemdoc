.. _utilisation:

.. index:: user-manual

============
Présentation générale
============

Le Système d'archivage électronique mutualisé est découpé en trois modules qui interagissent entre eux :

- gestion des données de référence
- gestion des processus d'archivage
- gestion de l'archivage et de la conservation

.. index:: Modules

Les modules du SAEM
-------------------

- gestion des données de référence ou référentiel : ce module permet de gérer les données relatives aux acteurs de l'archivage, aux vocabulaires contrôlés et aux profils de versement. I est géré par les archivistes.
- gestion des processus d'archivage ou ged sas : ce module http://www.alfresco.com permet de gérer les différents processus d'archivage (versement, consultation, restitution, élimination) dans un espace documentaire dédié. Il est utilisé par les services versants et les archivistes.
- gestion de l'archivage et de la conservation ou as@lae : ce module permet de conserver les archives numériques de façon pérenne. Il est utilisé par les archivistes.

.. note::

   Ces modules sont construits à partir d'applications open source. La gestion des processus est outillée par une version 5 d'Alfresco community, la gestion de l'archivage par as@lae en version 1.6 et la gestion des référentiels par une application développée sur la base du framework cubicweb_.

.. warning::

    Ces modules peuvent fonctionner de manière indépendante pour une implémentation dans d'autres systèmes. Leur utilisation combinée dans le SAEM permet de couvrir l'ensemble des exigences fonctionnelles de l'archivage électronique et de simplifier les tâches de gestion récurrentes.

L’intégration du module Référentiel au cœur de l’architecture du projet SAEM permet de centraliser l’ensemble des données de référence dans une seule et même brique applicative.
Dans un objectif d'interopérabilité, le protocole _OAI-PMH a été retenu pour réaliser les échanges de données par web services.
Ce protocole international étant un standard, il offre un moyen d'échanger et de moissonner des métadonnées d’archivage entre plusieurs institutions.
La GED SAS et Asalae récupèrent les données de référence suivantes depuis le référentiel :

•	Profils SEDA
•	Acteurs de l'archivage 
•	Vocabulaires contrôlés & concepts

**Vision cible du projet SAEM girondin**

.. image:: ../images/visionCible.png
   :height: 384px
   :width: 601px
   :scale: 80%
   :alt: la cible
   :align: center

Chaque module fait l'objet d'une page de présentation spécifique : 

* :ref:`Module de gestion des données de référence <referentiel>`
* :ref:`Module de gestion des processus d'archivage <gedsas>`
* :ref:`Module de gestion de l'archivage <asalae>`

Après un déploiement des applications, l'utilisation de l'ensemble des modules du projet SAEM implique :
•	d'enregistrer les données de référence de l'archivage dans le module gestion des données de référence
•	de synchroniser les modules gestion des processus d'archivage et gestion de l'archivage avec le module gestion des données de référence. 

.. _cubicweb: http://cubicweb.org/project/cubicweb-saem_ref
.. _documentation: https://wiki.alfresco.com/wiki/Scheduled_Actions#Cron_Explained
